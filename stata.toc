* OFF (to make site unavailable temporarily)
* lines starting with * are comments; they are ignored
* blank lines are ignored, too
* v indicates version—specify v 3, which is the current version of .toc files
v 3
* d lines display description text
* the first d line is the title, and the remaining ones are text
* blank d lines display a blank line
d mpitb - A toolbox for multidimensional poverty indices
d Nicolai Suppa, Centre for Demographic Studies (CED), Autonomous University of 
d Barcelona and Oxford Poverty and Human Development Initiative (OPHI), 
d University of Oxford
*d mpitb is a toolbox that offers several commands to facilitate specification, 
*d estimation, and analysis of multidimensional poverty indices (MPI) according 
*d to the global MPI workflow.

...
* l lines display links
* l word-to-show path-or-url [ description ]
* l word-to-show path-or-url [ description ]
...
* t lines display other directories within the site
* t path [ description ]
* t path [ description ]
* ...

* p lines display packages
p mpitb [ A toolbox for multidimensional poverty indices ]
*p pkgname [ description ]

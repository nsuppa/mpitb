**************************************
**# [I] level - manual estimation  ***
**************************************
* setup
cwf default
use mpitb_intro/cdta/ben_dhs17-18 , clear 
svyset psu [pw=weight] , strata(strata)	 singleunit(centered)

**************************************************************************
*** [I.1] levels: national and subgroup (w and w/o optional meta vars) ***
**************************************************************************
_mpitb_rframe, fr(level)
frame level : d			

svy : mean d_cm 
_mpitb_stores , fr(level) l(nat) m(hd) i(d_cm) sp(gmpi)

svy : mean d_nutr 
_mpitb_stores , fr(level) l(nat) m(hd) i(d_cm) sp(gmpi) w(equal) k(33)

svy : mean d_nutr , over(area)
_mpitb_stores , fr(level) l(area) m(hd) i(d_cm) spec(gmpi) 

frame level : l , noob		// 4 obs

*******************************************************
*** [I.2] levels (+ timestamps, + indicator) ***
*******************************************************
_mpitb_rframe, fr(level) ts
frame level : d

svy : mean d_cm 
_mpitb_stores , fr(level) l(nat) m(hd) sp(gmpi) w(equal) ts

svy : mean d_nutr 
_mpitb_stores , fr(level) l(nat) m(hd) i(d_cm) sp(gmpi) w(equal) k(33) ts

svy : mean d_nutr , over(area)
_mpitb_stores , fr(level) l(area) m(hd) i(d_cm) sp(gmpi) w(equal) k(33) ts

svy: prop area 
_mpitb_stores , fr(level) l(area) m(popsh) sp(gmpi) ts 

frame level : l , noob	sepby(meas)	// 6 obs

*********************************************************
*** [I.3] levels over time: national and by subgroups ***
*********************************************************
* setup 
tempvar tmp1 
use "mpitb_intro/cdta/sen_dhs05.dta", clear
gen t = 1 
append using mpitb_intro/cdta/sen_dhs17.dta , gen(`tmp1')
replace t = 2 if `tmp1' == 1 

svyset psu [pw=weight] , strata(strata)	 singleunit(centered)

_mpitb_rframe , fr(levot) t 
frame levot : d				// 15 vars

* national level over time 
svy : mean d_cm , over(t)
_mpitb_stores , fr(levot) l(nat) m(hd) i(d_cm) sp(gmpi) tvar(t)

* subgroup levels over time 
svy : mean d_cm , over(area t) 
_mpitb_stores , fr(levot) l(area) m(hd) i(d_cm) sp(gmpi) w(equal) tvar(t)

* national level over time more meta 
svy : mean d_cm , over(t)
_mpitb_stores , fr(levot) l(nat) m(hd) i(d_cm) sp(gmpi) w(equal) k(33) tvar(t)

frame levot : li , sepby(t) // 8 obs 

* single year national level 
_mpitb_rframe , fr(level)
svy : mean d_satt if sample == 1 & t == 1 
	_mpitb_stores , fr(level) l(nat) m(hd) ind(d_satt) sp(gmpi_cot)

* single year subgroup level 
svy : mean d_satt if sample == 1 & t == 1 , over(area)
	_mpitb_stores , fr(level) l(area) m(hd) sp(gmpi_cot) w(equal) ind(d_satt)

frame level : li 
	
* national over t 
_mpitb_rframe , fr(levot) t
svy : mean c_33_equal if sample == 1 , over(t) 	      
	_mpitb_stores , fr(levot) l(nat) m(hd) sp(gmpi_cot) w(equal) tvar(t) ind(d_satt) 

* proportion over time for subgroups 
egen mval = rowmiss(d_*) 
svy: prop area if mval == 0  , over(t)
_mpitb_stores , fr(levot) l(area) m(popsh) sp(gmpi_cot) tvar(t)

frame levot : li , noob 
	
***************************************
*** [II] changes: manual estimation ***
***************************************
tempvar tmp1 
use "mpitb_intro/cdta/sen_dhs05.dta", clear
gen t = 1 
append using mpitb_intro/cdta/sen_dhs17.dta , gen(`tmp1')
replace t = 2 if `tmp1' == 1 

svyset psu [pw=weight] , strata(strata)	 singleunit(centered)

mpitb set , d1(d_cm_01 d_nutr_01, name(hl)) d2(d_satt_01 d_educ_01, name(ed)) /// 
		d3(d_elct_01 d_sani_01 d_wtr_01 d_hsg_01 d_asst_01 d_ckfl_01 , name(ls)) name(gmpi_cot) replace
cap est drop _all
mpitb est , k(33) weights(equal) name(gmpi_cot) measures(M0) gen noestimate svy			

_mpitb_rframe , frame(cot) cot 
frame cot : d 

eststo M0: svy: mean c_33_equal if sample == 1 , over(t) 	 // exceptionally eststo as -nlcom- changes ereturn

* II.1 absolute raw change
lincom (c_33_equal@2.t - c_33_equal@1.t) 			
_mpitb_stores , fr(cot) l(nat) m(M0) k(33) ct(1) sp(gmpi_cot) w(equal) ann(0) yt0(2011) yt1(2014) t0(1) t1(2)  // abs 

* II.2 absolute ann change 
lincom (c_33_equal@2.t - c_33_equal@1.t)/3  
_mpitb_stores , fr(cot) l(nat) m(M0) k(33) ct(1) sp(gmpi_cot) w(equal) ann(1) yt0(2011) yt1(2014) t0(1) t1(2)  // abs ann

* II.3 relative raw change 
nlcom (_b[c_33_equal@2.t] - _b[c_33_equal@1.t]) / _b[c_33_equal@1.t] , post 
_mpitb_stores , fr(cot) l(nat) m(M0) k(33) ct(2) sp(gmpi_cot) w(equal) ann(0) yt0(2011) yt1(2014) t0(1) t1(2) // rel
est res M0 

* II.4 relative ann change 		
nlcom (_b[c_33_equal@2.t] / _b[c_33_equal@1.t])^(1/3)-1 , post  			
_mpitb_stores , fr(cot) l(nat) m(M0) k(33) ct(2) sp(gmpi_cot) w(equal) ann(1) yt0(2011) yt1(2014) t0(1) t1(2) // rel ann 

frame cot : li			// 4 obs 
		
* II.5 subgroup (abs, rel, ann, raw)
eststo M0_area: svy : mean c_33_equal if sample == 1 , over(t area) 	        // eststo exceptionally to recover after -nlcom-
	_mpitb_rframe , frame(level) t 												// related levels might be useful for debug too
	_mpitb_stores , fr(level) l(area) m(M0) ct(0) sp(gmpi_cot) w(equal) tvar(t)	

	_mpitb_rframe , frame(cot) cot 
	* changes
	forval s = 0/1 {
		
		* abs 
		lincom (c_33_equal@`s'.area#2.t - c_33_equal@`s'.area#1.t) 		
		_mpitb_stores , fr(cot) l(area) m(M0) k(33) ct(1) sp(gmpi_cot) w(equal) ann(0) yt0(2011) yt1(2014) t0(1) t1(2)

		* ann
		lincom (c_33_equal@`s'.area#2.t - c_33_equal@`s'.area#1.t)/3 	
		_mpitb_stores , fr(cot) l(area) m(M0) k(33) ct(1) sp(gmpi_cot) w(equal) ann(1) yt0(2011) yt1(2014) t0(1) t1(2) subg(`s') 

		* rel
		nlcom (_b[c_33_equal@`s'.area#2.t] - _b[c_33_equal@`s'.area#1.t]) / _b[c_33_equal@`s'.area#1.t] , post 
		_mpitb_stores , fr(cot) l(area) m(M0) k(33) ct(2) sp(gmpi_cot) w(equal) ann(0) yt0(2011) yt1(2014) t0(1) t1(2) subg(`s') 
		est res M0_area
		
		* rel ann 
		nlcom (_b[c_33_equal@`s'.area#2.t] / _b[c_33_equal@`s'.area#1.t])^(1/3)-1 , post 
		_mpitb_stores , fr(cot) l(area) m(M0) k(33) ct(2) sp(gmpi_cot) w(equal) ann(1) yt0(2011) yt1(2014) t0(1) t1(2) subg(`s') 
		est res M0_area
	}

frame cot : li ,  sepby(loa ctype)	// 8 obs
exit 

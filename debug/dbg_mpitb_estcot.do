
frame reset 

* setup 
tempvar tmp1 tmp2 

use mpitb_intro/cdta/sen_dhs05.dta , clear
gen t = 1 
append using mpitb_intro/cdta/sen_dhs17.dta , gen(`tmp1')
append using mpitb_intro/cdta/sen_dhs19.dta , gen(`tmp2')

replace t = 2 if `tmp1' == 1 
replace t = 3 if `tmp2' == 1

recode t (1=2005) (2=2017) (3=2019), gen(year_cot)

svyset psu [pw=weight] , strata(strata)	 singleunit(centered)	


_mpitb_rframe , frame(cot) cot 
svy : mean d_satt_01  , over(t)
_mpitb_estcot , frame(cot) timevar(t) total m(hd) sp(gmpi_cot) y(year_cot) ind(d_satt)

svy : mean d_satt_01, over(t area) 	      
_mpitb_estcot , frame(cot) sub(area) timevar(t) total m(hd) sp(gmpi_cot) ind(d_satt) y(year_cot)

frame cot : li , noobs
frames drop cot

* feature: total and insequence 
_mpitb_rframe , frame(cot) cot 
svy : mean d_satt_01  , over(t)

_mpitb_estcot , frame(cot) timevar(t) inseq m(hd) sp(gmpi_cot) ind(d_satt) y(year_cot)

* feature: subgroup 

* feature: -noann- and -noraw-
_mpitb_rframe , frame(cot) cot 
svy : mean d_satt_01 , over(t)
_mpitb_estcot , frame(cot) timevar(t) total m(hd) sp(gmpi_cot) y(year_cot) noann
frame cot : tab ann  

_mpitb_estcot , frame(cot) timevar(t) total m(hd) sp(gmpi_cot) y(year_cot) noraw noann


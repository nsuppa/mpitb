version 17 

frame reset 
use data/syn_cdta.dta , clear 

svyset psu [pw=weight], strata(stratum)

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

* mpi 

* mpitb est , m(all) name(mympi) levelfr(myres, replace) svy k(33) w(equal) cotm(all) cotfr(mycots, replace) tvar(t) coty(t)
	
mpitb est , m(all) name(mympi) svy k(33) w(equal) over(area region) /// 
	cotm(all) tvar(t) coty(t) levelfr(myres, replace) cotfr(mycots, replace) 
	
	levelsa(myres, replace) cotsa(mycots, replace) 	
	
	
	
cwf myres
table (measure) (loa) 
foreach m in k wgts spec {
	qui levelsof `m' , loc(llist)
	di "# `m': " _col(10) `: word count `llist''
}


	
* option for wgts name
mpitb est , m(all) k(33) n(mympi) w(dimw(.5 .25 .25) name(health50)) svy estclear 	// dimensional weights with name for wgts
mpitb est , m(all) k(33) n(mympi) w(equal name(eqdim)) svy estclear			// equal weights with name for wgts
mpitb est , m(all) k(33) n(mympi) w(indw(.1 .1 .1 .1 .1 .1 .1 .1 .1 .1) name(eqind)) svy estclear // indw() and name()

* dimensional weights
mpitb est , m(all) k(33) n(mympi) w(dimw(.5 .25 .25)) svy 				// dimensional weights

mpitb show , n(mympi)


* indicator level weights
est clear
set traced 2
set trace on
capture drop sample



est clear


mpitb est , m(all) k(33) n(mympi) w(indw(.5 .4 .1) name()) wgtsname() svy noestimate

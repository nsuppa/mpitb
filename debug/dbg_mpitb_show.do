cscript "cert {bf:mpitb show}" adofiles _mpitb_show.ado

* toy data for testing
est clear

clear
set obs 1000
capture drop d_*
foreach v in  d_nutr d_cm d_satt d_educ d_elct d_sani d_wtr d_hsg d_ckfl d_asst {
	gen `v'= 1 
}
gen weight = 1
gen strata = 1
gen psu = 1

use data/syn_cdta , clear 

* all indicator w/o weights
mpitb set , d1(d_cm d_nutr , name(hl)) d2(d_satt d_educ, name(ed) ) d3(d_elct d_sani /// 
	d_wtr d_hsg d_ckfl d_asst , name(ls)) name(mympi) 
mpitb show , name(mympi)

svyset psu [pw=weight], strata(stratum)

mpitb est , m(H) k(33) w(equal) n(mympi) svy levelfr(myres)

mpitb show , name(mympi)


* missing indicator
est clear
replace d_elct = .
mpitb set , d1(d_cm d_nutr , name(hl)) d2(d_satt d_educ, name(ed) ) d3(d_elct d_sani /// 
	d_wtr d_hsg d_ckfl d_asst , name(ls)) name(mympi) replace

mpitb est , m(H) k(33) w(equal) n(mympi) svy levelfr(myres, replace)

mpitb show , name(mympi)


* several specs
mpitb set , d1(d_cm d_nutr , name(hl)) d2(d_satt d_educ, name(ed) ) d3(d_elct d_sani /// 
	d_wtr d_hsg , name(ls)) name(compact_LS) desc(compact living standards) replace

mpitb set , d1(d_cm , name(hl)) d2(d_educ, name(ed) ) d3(d_elct d_sani /// 
	d_wtr d_hsg d_ckfl d_asst , name(ls)) name(redu_hled) /// 
	desc(compact health and living standard dimensions) replace

exit



* (A) program is found
mpitb set

* (B) syntax and other error messages
rcof n mpitb set , d1(d_cm , name(hl)) d2(d_satt , name(ed) ) d3(d_elct, name(ls)) /// 
	name(t01) store clear replace == 198

rcof n mpitb set , d1(d_cm , name(hl)) d2(d_satt , name(ed) ) d3(d_elct , name(ls)) name(t01) store == 198




* (C) use cases

mpitb set , d1(d_cm, name(hl)) d2(d_satt, name(ed) ) d3(d_elct, name(ls)) name(t01) store

char li


mpitb set , d1(d_cm, name(hl)) d2(d_satt, name(ed)) d3(d_elct, name(ls)) name(t01) store replace

* check adding and ordering specs
mpitb set , clear
mpitb set  , d1(d_cm, name(hl)) d2(d_satt, name(ed)) d3(d_elct, name(ls)) name(t01) store 
mpitb set  , d1(d_cm, name(hl)) d2(d_satt, name(ed)) d3(d_elct, name(ls)) name(t03) store 
mpitb set  , d1(d_cm, name(hl)) d2(d_satt, name(ed)) d3(d_elct, name(ls)) name(t02) store 

char l

* desc
mpitb set , d1(d_cm, name(hl)) d2(d_satt, name(ed)) d3(d_elct, name(ls)) name(t01) /// 
	desc(minimal specification: only 1 indicator per dimension) store replace
char l

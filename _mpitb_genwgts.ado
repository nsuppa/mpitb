*! part of -mpitb- the MPI toolbox

cap program drop _mpitb_genwgts
program define _mpitb_genwgts , rclass
	syntax , [Ndim(integer 3) Step(real .1) Mat(name) minmaxwgt(numlist min=2 max=2 >=0 <=1 ascending) /// 
	 nonzero Distance(numlist min=1 max=1 >=0 <=1) clear]

	/* check for option combinations
	 minmax  |  nonzero  |   distance
	 */

	if "`minmaxwgt'" != "" loc minwgt : word 1 of `minmaxwgt'
	else loc minwgt 0
	if "`minmaxwgt'" != "" loc maxwgt : word 2 of `minmaxwgt'
	else loc maxwgt 1


	if ("`clear'" == "") preserve
	clear

	numlist "`minwgt'(`step')`maxwgt'"
	loc nlist `r(numlist)'
	loc N :	word count `nlist'
	qui set obs `N'

	if `N' == 1 {
		di as err "only 1 weighting scheme specified!"
		err 198
	}

	qui gen d1 = .
	loc i 1
	foreach n of numlist `nlist' {
		qui replace d1 = `n' in `i++' 
	}
	forval d=2/`ndim' {
		gen d`d'= d1
	}

	fillin d* 			// create all combinations

	egen double sow = rowtotal(d*)		// sum of weights
	qui keep if float(sow) == 1
	if "`nzero'" != "" {
		egen zerowgt = anymatch(d*) , v(0)
		drop if zerowgt == 1
	}
	if "`distance'" != "" {
		loc eqw = 1/`ndim'
		loc s (`eqw'-d1)^2
		forval d =2/`ndim' {
			loc s `s' + (`eqw'-d`d')^2
		}
		gen es = sqrt(`s')
		drop if es >= `distance'
	}

	foreach v of varlist d* {
		assert !mi(`v')
	}

	loc Nw = _N			// # of wgts
	drop _fillin sow 		
	mkmat  d* , mat(W)

	qui replace d1 = d1 * 100				// create and store wgtsnames
	gen name = strofreal(float(d1),"%03.0f")
	loc ndim 3
	forval d=2/`ndim' {
		qui replace d`d' = d`d' * 100
		qui replace name = name + strofreal(float(d`d'),"%03.0f")
	}
	qui levelsof name , l(wgtsnames) c
	mat rown W = `wgtsnames'

	* report back
	di as txt "Matrix with " as res `Nw' as txt " weighting schemes for " as res `ndim' as txt " dimensions generated (step=" as res `step' as text")."

	if ("`mat'" != "") mat `mat' = W

	* return
	ret loc step `step'
	ret loc Nwgts `Nw'
	ret loc Ndim `ndim'
	ret mat allwgts = W
	if "`clear'" == "" restore

end

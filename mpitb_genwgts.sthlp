{smcl}
{* *! version 0.1  21 Dec 2021}{...}
{vieweralsosee "mpitb" "help mpitb"}{...}
{viewerjumpto "Syntax" "mpitb_genwgts##syntax"}{...}
{viewerjumpto "Description" "mpitb_genwgts##description"}{...}
{viewerjumpto "Options" "mpitb_genwgts##options"}{...}
{viewerjumpto "Remarks" "mpitb_genwgts##remarks"}{...}
{viewerjumpto "Examples" "mpitb_genwgts##examples"}{...}
{p2colset 1 18 20 2}{...}
{p2col:{bf:mpitb genwgts} {hline 2}} generates specific weighting schemes{p_end}
{p2colreset}{...}

{marker syntax}{...}
{title:Syntax}

{p 8 15 2}
{cmd:mpitb genwgts , }[{it:options}]

{synoptset 22 tabbed}{...}
{synopthdr:options}
{synoptline}
{synopt :{opt n:dim(numlist)}}number of dimensions{p_end}
{synopt :{opt s:tep(real)}}real number representing the step to obtain grid{p_end}
{synopt :{opt minmaxwgt(numlist)}}upper and lower bound for weights range{p_end}
{synopt :{opt nonzero}}restrict generated weighting schemes to those containing 
non-zero values for every dimension{p_end}
{synopt :{opt distance(numlist)}}restricts the generated weighting schemes to those,
whose eucledian distance is smaller than the value provided{p_end}
{synopt :{opt clear}}clears existing data, if present{p_end}
{synoptline}
{p2colreset}{...}
{p 4 6 2}* required options{p_end}

{marker description}{...}
{title:Description}

{pstd}
{cmd:mpitb genwgts} generates sets of alternative weighting schems. {p_end}


{marker options}{...}
{title:Options}

{phang}
{opth ndim(numlist)} number of dimensions, for which weighting schemes are to be 
generated.{p_end}

{phang}
{opt ....} ... {p_end}

{marker remarks}
{title:Remarks}

{pstd}
The currently implemented procedure for generating weight grids is inefficient. A 
more efficient algorithm is already available, but not yet implemented.{p_end} 

{marker examples}
{title:Examples}

{pstd}
...{p_end}

{phang2}
{cmd:mpitb genwtgs , ...  } {p_end}




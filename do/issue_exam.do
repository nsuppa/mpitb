
**# issue 10: generated variables may already exist
use "~/Work/Development/mpitb/data/syn_cdta.dta" , clear
svyset psu [pw=weight], strata(stratum)

* (1) gafvars 
* problen with ind-spec vars
mpitb gafvars , k(33 )indvars(d_cm d_nutr d_satt) indw(.5 .25 .25) wgtsid(eql) ind
mpitb gafvars , k(33 )indvars(d_cm d_nutr d_satt) indw(.5 .25 .25) wgtsid(eql) ind replace

use "~/Work/Development/mpitb/data/syn_cdta.dta" , clear
svyset psu [pw=weight], strata(stratum)

* problem with cvec
mpitb gafvars , k(33 )indvars(d_cm d_nutr d_satt) indw(.5 .25 .25) wgtsid(eql) cvec
mpitb gafvars , k(33 )indvars(d_cm d_nutr d_satt) indw(.5 .25 .25) wgtsid(eql) cvec replace

* (2) mpitb est 
use "~/Work/Development/mpitb/data/syn_cdta.dta" , clear
svyset psu [pw=weight], strata(stratum)

mpitb set , name(trial01) desc(preferred spec) ///
		d1(d_cm d_nutr, name(hl)) /// 
		d2(d_satt d_educ, name(ed)) /// 
		d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

mpitb est , n(trial01) m(all) k(33) lfr(ind_equal, replace) ///
	w(indw(.1 .1 .1 .1 .1 .1 .1 .1 .1 .1) name(ind_equal)) svy gen 

mpitb est , n(trial01) m(all) k(33) lfr(ind_equal, replace) ///
	w(indw(.1 .1 .1 .1 .1 .1 .1 .1 .1 .1) name(ind_equal)) svy replace

* pctb-spec 
mpitb est , n(trial01) m(all) indm(hdk pctb) aux(hd mv) k(33) lfr(ind_equal, replace) ///
	w(indw(.1 .1 .1 .1 .1 .1 .1 .1 .1 .1) name(ind_equal)) svy gen replace


exit 

**# issue 52: wrong dimensional weights displayed if indicator weights are specified
use "~/Work/Development/mpitb/data/syn_cdta.dta" , clear
svyset psu [pw=weight], strata(stratum)

mpitb set , name(trial01) desc(preferred spec) ///
		d1(d_cm d_nutr, name(hl)) /// 
		d2(d_satt d_educ, name(ed)) /// 
		d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))


mpitb est , n(trial01) m(all) k(33) lfr(ind_equal, replace) ///
	w(indw(.1 .1 .1 .1 .1 .1 .1 .1 .1 .1) name(ind_equal)) svy 

mpitb est , n(trial01) m(all) k(33) lfr(ind_equal, replace) ///
	w(dimw(.5 .25 .25) name(ind_equal)) svy 



**# issue 21: identify lincom, nlcom 

use "~/Work/Development/mpitb/data/syn_cdta.dta" , clear
svyset psu [pw=weight], strata(stratum)
svy: mean d_cm d_nutr
eret li

lincom _b[d_cm] - _b[d_nutr] 

nlcom _b[d_cm] - _b[d_nutr] , post 

**# issue 50: examine and improve string length issue in results frame 
frame reset 
use "~/Work/Development/mpitb/data/syn_cdta.dta" , clear

mpitb rframe , fr(myres)

mean d_cm
mpitb stores , fr(myres) loa(nat) m(xyz) spec(mympi) i(d_cm)
frame myres: d	// all str10

mean d_cm
mpitb stores , fr(myres) loa(nat) m(xyz) spec(mympi_sdfdsdfg_sdf) i(this_is_my_too_long_name)
frame myres: li
frame myres: d	// ind and spec longer as needed

mean d_cm
mpitb stores , fr(myres) loa(nat) m(xyz) spec(mympi) i(d_cm) // posting sth short again
frame myres: d	

* testing 
* cwf myres 
loc loa "regional_also_too_lng"
loc measure "my_too_long_name"

foreach l in loa measure {
	frame myres {
		loc fvarl = substr("`: type `l''",4,.) 	// get frame var length
		loc pvall = ustrlen("``l''")			// get posted val length 
		if `pvall' > `fvarl' {
			di "recast of `l' to `pvall' needed"	
		recast str`pvall' `l'
		}
	}
}

**# set varabbrev 
frame reset 
set varabbrev off

use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

mpitb est , name(mympi) m(M0) indm(actb pctb) k(33) w(equal) svy lfr(myresults) // no problem 




**# issue 42 
frame reset 
use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

mpitb est , name(mympi) m(M0) k(33) w(equal) noestimate svy // no problem 

mpitb est , name(mympi) m(M0) k(33) w(equal) noestimate svy lfr(myres, replace) // problem 
	// no obs error 2000
	
**# issue 41: 
use data/syn_cdta.dta , clear 
sum 
svyset psu [pw=weight], strata(stratum)

gen nomiss = !mi(d_nutr, d_cm, d_satt, d_educ, d_elct, d_sani, d_wtr, d_hsg, d_ckfl, d_asst )
count if nomiss==1

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

	*set traced 6 
	*set trace on 
mpitb est , name(mympi) m(M0) indm(all) k(33) w(equal) lfr(myres, replace) svy gen 

sum I_* c_* sample		// sample correct, but c_* and I_* not

sum cd* actb* 

mata: A = (4, . , 10, 7)
mata: mm_cond(A :< 5, . , 0)

mata: A = (.5 \ .4 \ .1 \ . \1 )
mata: A
mata: B = (A :< .)  		// non-mv matrix 	
mata B 

(A :< .)  		// MV matrix 
	//nmv[.,.] = (c :< .)  		// non-mv matrix 	

mata: B
mata: A :/ B

mata: mata clear


**# issue 39: 
use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

mpitb est , name(mympi) m(H) k(33) w(equal) lfr(myres, replace) svy cotm(H hdk) /// 
	cotfr(myres2, replace) tvar(t) cotyear(year) 
 


**# issue YY: precision and rounding related to counting vector
use data/syn_cdta.dta if t == 1 , clear 
svyset psu [pw=weight], strata(stratum)

gen d_x1 = rbinomial(1,.25)
gen d_x2 = rbinomial(1,.10)

gen d_y1 = rbinomial(1,.20)
gen d_y2 = rbinomial(1,.10)
gen d_y3 = rbinomial(1,.15)

mpitb set , name(mympi) d1(d_cm d_nutr d_x1 d_x2, name(hl)) d2(d_satt d_educ d_y1 d_y2 d_y3 , name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

	/* 	1/15 = .06666667
		1/10 = .1 
		2/15 = .13333333
		0.2/6 = .03333333
		0.3/4 = 0.075
		0.1/4 = 0.025 
	*/
loc w1 = float(1/15)
loc w2 = float(2/15)
loc w3 = float(.2/6)

mpitb est , name(mympi) m(H) k(20) w(indw(`w1' `w1' `w1' `w1' .1 `w2' `w3' `w3' `w3' `w3' `w3' `w3' 0.075 0.025 0.2) name(myw) ) /// 
	lfr(myres, replace) svy gen

tab c_myw I_20_myw

**# issue 36: returns of -mpitb assoc-
use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)

mpitb assoc , dep(d_*)

ret li
mat dir 
	

**# issue #15: -nooverall- and empty region results in syntax error
use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)
mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

set traced 3
set trace off
mpitb est , name(mympi) m(M0) indm(pctb) k(33) w(equal) levelfr(myres, replace) /// 
	svy over(, nooverall)

frame myres : tab loa



**# issue 14: -noestimate- and non-empty -over()- results in empty over_vars
use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)
mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

set traced 3
set trace on
mpitb est , name(mympi) m(M0) indm(pctb) k(33) w(equal) levelfr(myres, replace) /// 
	svy noestimate over(region)
	/* `over_varlist' empty 
		=> subsequent loop of varlist reports error (which seeks to count the number of estimates)
		reason: condition prior to loop checks on `over' and not `over_varlist' local
	*/ 

	mpitb est , name(mympi) m(M0) indm(pctb) k(33) w(equal) levelfr(myres, replace) /// 
	svy noestimate 


	
* issue #7 
use data/syn_cdta.dta , clear  
mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

svyset psu [pw=weight], strata(strata)

mpitb est , aux(all) n(mympi) svy levelfr(mylevs, replace)
mpitb est , m(all) n(mympi) svy levelfr(mylevs, replace)

	/*
	- conditional syntax check already there, but options were accidentally still required.
	== SOLVED ==
	*/

**# issue #6: pctb w/o actb produces error
use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)
mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

mpitb est , name(mympi) m(M0) indm(pctb) k(33) w(equal) levelfr(myres, replace) svy ///

frame myres: tab measure
frame myres: li ind b se if  measure == "pctb"
	// conclusion: issue not replicable!

**# issue #5: variables for actb are produced even if only -hdk- is set?
use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)
mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

mpitb est , name(mympi) m(M0) indm(actb) k(33) w(equal) levelfr(myres, replace) svy gen 

d *d_cm*

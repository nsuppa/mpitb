**# check rframe 

frame reset 
_mpitb_rframe , fr(lev) t
frame lev : set obs 1
frame lev: li 
frame lev: ds 
ret li
char li

frame reset 
_mpitb_rframe , fr(lev) add(ccty)
cwf lev
char _dta[useradded] ccty 
char li

***** net from and other installation stuff ******

cd /home/nico/Work/scratch

net from "/home/nico/Work/Development/mpitb"

net describe mpitb

net set ado myado

net install mpitb

***************************************************

***** show versions of all ados *******

loc flist : dir . files "*ado"
foreach f in `flist' {
	which `f'
}

****************************************


****** noisily / qui tests ******
use data/syn_cdta.dta , clear  

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

svyset psu [pw=weight], strata(stratum)

set traced 3
set trace on
mpitb est , m(all) aux(all) indm(all) over(area) k(33) w(equal) n(mympi) svy levelfr(myres, replace) /// 
	cotm(all) cotfr(mycots, replace) tvar(t) cotyear(year)


cap program drop myprog 
program define myprog 
	syntax [, Verbose ]

if "`verbose'" == "" {
	loc qui qui
}
`qui' mean d_cm

if "`verbose'" != "" {
	sum d_*
}

tab d_cm d_nutr 

di in smcl "this is a message"
di as err "this is and error message"

end 


myprog , v


**************************************************

use data/syn_cdta.dta , clear  

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

mpitb set , name(mympi1) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))


mpitb set , clear






****** version tests *******
capture program drop myprog 
program define myprog 
	version 17
	di "Hello world!"
end

version  : myprog



exit  



*** mpitb show tests

use data/syn_cdta.dta , clear  

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))
	
svyset psu [pw=weight], strata(stratum)

mpitb est , m(all) k(33) w(equal) n(mympi) svy levelfr(myres, replace)

mpitb show , 
name(mympi)

exit




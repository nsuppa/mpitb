/* This file creates synthetic deprivation data. This data may be used for 
	- examples and introductory script 
	- bugfixing  and error descriptions
	- certification scripts 
*/ 

version 17


********************************
**# (A) synthetic micro data ***
********************************

********************
**# A.1 COT data ***
********************
clear all 
set seed 1234567890
set obs 7500
capture drop d_*
 
gen d_nutr1  = rbinomial(1,.25)
gen d_nutr2  = rbinomial(1,.21)
gen d_cm1  = rbinomial(1,.06)
gen d_cm2  = rbinomial(1,.04)
gen d_satt1  = rbinomial(1,.32)
gen d_satt2  = rbinomial(1,.28)
gen d_educ1  = rbinomial(1,.3)
gen d_educ2  = rbinomial(1,.25)
gen d_elct1  = rbinomial(1,.4)
gen d_elct2  = rbinomial(1,.35)
gen d_sani1  = rbinomial(1,.23)
gen d_sani2  = rbinomial(1,.18)
gen d_wtr1  = rbinomial(1,.27)
gen d_wtr2  = rbinomial(1,.24)
gen d_hsg1  = rbinomial(1,.41)
gen d_hsg2  = rbinomial(1,.34)
gen d_ckfl1  = rbinomial(1,.15)
gen d_ckfl2  = rbinomial(1,.12)
gen d_asst1  = rbinomial(1,.28)
gen d_asst2  = rbinomial(1,.21)

* data imperfections
*replace d_cm = .		// missing indicator
replace d_nutr1 = . in 80/140	// non-response
replace d_nutr2 = . in 150/170	
replace d_satt1 = . in 130/145
replace d_satt2 = . in 160/180

* soc dem
gen region = floor(runiform(1,21))
gen area = rbinomial(1,.6)
* gen agegrp = .

* svy design
gen stratum = 100 * region + round(runiform(0,5))
gen psu = 1000 * stratum + round(runiform(0,5))
gen weight = 1

gen id = _n
reshape long d_nutr@ d_cm@ d_satt@ d_educ@ d_elct@ d_sani@ d_wtr@ d_hsg@ d_ckfl@ d_asst@ , i(id) j(t)
drop id

recode t (1=2010)(2=2018.5) , gen(year)

order d_* 

* label
levelsof region , c l(llist)
foreach l of loc llist {
	lab def region `l' "region `l'" , modify 
}
lab val region region 
lab def area 0 "rural" 1 "urban"  , modify 
lab val area area 

* tidy up
order d_* area region stratum psu weight year t 

loc clist : char _dta[]
foreach c of local clist {
	char _dta[`c'] ""			
}

bys t : sum 
compress 

save data/syn_cdta.dta , replace  		// to be used for COT

***********************
**# A.2 simple cdta ***
***********************
* 1. country
preserve 
keep if t == 1
drop t year 
char _dta[ccty] "ABC" 
char _dta[ccnum] "998" 
char _dta[survey] "DHS" 
char _dta[year] "2010"
char _dta[cty] "ABC country"

* region label
levelsof region , c l(llist)
foreach l of loc llist {
	lab def region `l' "ABC - region `l'" , modify 
}
lab val region region
save data/syn_ABC_cdta.dta , replace 
restore 

* 2. country 
keep if t == 2
drop t year
char _dta[ccty] "DEF" 
char _dta[ccnum] "999" 
char _dta[survey] "MICS" 
char _dta[year] "2018-2019"
char _dta[cty] "DEF country"

* region label
levelsof region , c l(llist)
foreach l of loc llist {
	lab def region `l' "DEF - region `l'" , modify 
}
lab val region region
save data/syn_DEF_cdta.dta , replace 


* 3. country 
use data/syn_DEF_cdta.dta , clear  

replace region = .

char _dta[ccty] "GHI" 
char _dta[ccnum] "997" 
char _dta[survey] "DHS" 
char _dta[year] "2015"
char _dta[cty] "GHI country"

save data/syn_GHI_cdta.dta , replace 


************************************************************************************
**# A.3 copy dta files into intro folder (to be shared as aux files for toolbox) ***
************************************************************************************

* basic level and cot example 
copy data/syn_cdta.dta mpitb_intro/syn_cdta.dta , replace 

* simple cross-country example 
foreach d in ABC DEF GHI {
	copy data/syn_`d'_cdta.dta mpitb_intro/cdta/syn_`d'_cdta.dta , replace 
}


*********************************
*** synthetic reference sheet ***
*********************************

clear all 
set obs 23
input str3 ccty 
AFG
ARM
AGO
BGD
CHN
COL
CUB
DZA
ECU
KAZ
KHM
MEX
IND
IDN
JOR
PER
RWA
UGA
TUN
TZA
ZZZ
end 

mpitb undpwr wr , c(ccty) ignore 
compress 

save data/syn_refsheet.dta , replace 

exit 

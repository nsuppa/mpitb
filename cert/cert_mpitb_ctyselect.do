cscript "cert {bf:mpitb ctyselect}" adofiles _mpitb_ctyselect.ado

******************
**# [A] syntax ***
******************

use data/syn_refsheet , clear  

* varlist required 
rcof n _mpitb_ctyselect == 100 

* choose one of sel, rexp or wr
rcof n _mpitb_ctyselect ccty , s(IND) r(I*) == 197 

* country not found 
rcof n _mpitb_ctyselect ccty , s(XYZ) == 198 

* invalid world region 
rcof n _mpitb_ctyselect ccty , wr(AS XCV) == 198
 
******************
**# [B] output ***
******************

* selected all countries 
rcof n _mpitb_ctyselect ccty == 0 				
assert `r(Nctylist)' == 21

* repeated entries 
rcof n _mpitb_ctyselect ccty , s(IND IND) == 0	
assert `r(Nctylist)' == 1

* manual select
_mpitb_ctyselect ccty ,  s(AFG TZA)
assert "`r(ctylist)'" == "AFG TZA"
assert `r(Nctylist)' == 2

* regular expression select
_mpitb_ctyselect ccty ,  r(^[I])
assert "`r(ctylist)'" == "IDN IND"

* countries from single world region 
_mpitb_ctyselect ccty ,  wr(SSA)
assert "`r(ctylist)'" == "AGO RWA TZA UGA"

* countries from single world region 
_mpitb_ctyselect ccty ,  wr(SSA SA)
assert "`r(ctylist)'" == "AGO RWA TZA UGA AFG BGD IND"

* warning: countries w/o world region
n _mpitb_ctyselect ccty , wr(AS) 
 
exit 

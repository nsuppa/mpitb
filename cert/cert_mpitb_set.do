cscript "cert {bf:mpitb stores}" adofiles _mpitb_stores.ado

use data/syn_cdta.dta , clear 

******************
**# [A] syntax ***
******************

rcof "n _mpitb_set" == 198 									// name() or clear required
rcof "n mpitb set, n(mympi) d1(d_cm d_nutr) clear" == 198 	// name() or clear required
rcof "n _mpitb_set, n(mympi)" == 198 						// d1()  required with name

* variable not found
rcof "n mpitb set, n(mympi) d1(d_cm XYZ)" == 111	

* too long mpi name 
rcof n mpitb set, n(mympiwithalongname) d1(d_cm d_nutr) == 198 

* too long dimension name 
rcof n mpitb set, n(mympi) d1(d_cm d_nutr, name(verylongdimensionname)) replace == 198 

* max mpi and dim names (=> no error)
rcof "n mpitb set, n(mpiwithten) d1(d_cm d_nutr, name(dimewi)) replace" == 0 

* long variable names 
foreach v of varlist d_* {
	clonevar `v'_myverylong = `v'
}
unab vlist : *_myverylong
rcof n mpitb set, n(mpiwithten) d1(`vlist' , n(dimewith)) replace == 198 

* spec already exists 
mpitb set, n(mympi) d1(d_cm d_nutr)
rcof "n mpitb set, n(mympi) d1(d_cm d_nutr)" == 198 

* one of clear or replace 
rcof "n mpitb set, n(mympi) d1(d_cm d_nutr) clear replace" == 198 

* repeated deprivation vars 
rcof "n mpitb set, n(mympi) d1(d_cm d_nutr) d2(d_satt d_nutr) replace" == 198 

* too many names
rcof "n mpitb set, n(mympi) d1(d_cm d_nutr, name(sdf sdf)) d2(d_satt d_educ) replace" == 103

* non-existing dimension 
rcof "n mpitb set, n(mympi) d1(d_cm) d2(d_satt d_nutr) d11(d_sani) replace" == 198 

* invalid name
rcof "n mpitb set, n(mympi) d1(d_cm d_nutr, name(123)) d2(d_satt d_educ) replace" == 198

**# A.1 data checks 
replace d_cm = -1 in 20/40
rcof "n mpitb set, n(mympi) d1(d_cm d_nutr, name(abs)) d2(d_satt d_educ) replace" == 198

replace d_cm = 0 in 20/40
replace d_nutr = 0.5 in 20/40
rcof "n mpitb set, n(mympi) d1(d_cm d_nutr, name(abs)) d2(d_satt d_educ) replace" == 198
replace d_nutr = 0 in 20/40

**# [A.2] inputs ** 

* all 10 dimensions
mpitb set, n(mympi) d1(d_cm) d2(d_sani) d3(d_hsg) d4(d_satt) d5(d_educ) d6(d_ckfl) d7(d_asst) /// 
	d8(d_nutr) d9(d_elct) d10(d_wtr) replace  

mpitb est , n(mympi) m(all) lfr(myres, replace) k(33) w(equal) 

******************
**# [B] Output ***
******************

* replace and clear are effective 
mpitb set, n(mympi) d1(d_cm d_hsg) d2(d_satt d_educ) replace  
mpitb set, n(mympi) d1(d_cm d_nutr) d2(d_satt d_educ) replace  
assert "`: char _dta[MPITB_mympi_dim_d1_vars]'" == "d_cm d_nutr"

exit

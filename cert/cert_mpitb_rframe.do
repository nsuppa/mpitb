cscript "certifying {bf:mpitb rframe}" adofiles _mpitb_rframe.ado

frame reset 

* variables supposed to exist 
loc cert_core b se ll ul pval tval loa measure indicator spec wgts k ctype subg 
loc cert_ts ts_est ts_data 
loc cert_tvars t
loc cert_cot yt0 yt1 t0 t1 ann 

* syntax and error message checks
rcof "noi _mpitb_rframe" == 198 
rcof "noi _mpitb_rframe , fr(level) cot t" == 197 
rcof "noi _mpitb_rframe , fr(level) add(asd asd)" == 103
rcof "noi _mpitb_rframe , fr(level) add(3asd)" == 198

* frame already exists 
_mpitb_rframe , fr(lev) 
rcof "n _mpitb_rframe , fr(lev) " == 110
rcof "n _mpitb_rframe , fr(lev) replace " == 0

* frame is replaced 
_mpitb_rframe , fr(lev) replace

* syntax - no err msgs
rcof "noi _mpitb_rframe , fr(level) replace cot" == 0 
rcof "noi _mpitb_rframe , fr(level) replace t" == 0
rcof "noi _mpitb_rframe , fr(level) replace ts" == 0
rcof `"noi _mpitb_rframe , fr(level) replace add("sdfsd")"' == 0 

* checks: # vars and names expected  
_mpitb_rframe , fr(lev) replace
frame lev: assert c(k) == 14				
frame lev: conf var `cert_core' , ex

_mpitb_rframe , fr(levtime) ts replace 
frame levtime : assert c(k) == 16 
frame levtime : conf var `cert_core' `cert_ts' , ex

_mpitb_rframe  , fr(levot) t replace 
frame levot : assert c(k) == 15
frame levot : conf var `cert_core' `cert_tvars' , ex

_mpitb_rframe  , fr(cot) cot replace 
frame cot : assert c(k) == 19
frame cot : conf var `cert_core' `cert_cot' , ex 

_mpitb_rframe  , fr(levadd) add(ccty) replace 
frame levadd : assert c(k) == 15
frame levadd : conf var `cert_core' ccty , ex 

_mpitb_rframe  , frame(cotadd) cot add(ccty) replace 
frame cotadd : assert c(k) == 20
frame cotadd : conf var `cert_core' `cert_cot' ccty , ex 

_mpitb_rframe  , frame(cotaddt) cot add(ccty) ts replace 
frame cotaddt : assert c(k) == 22
frame cotaddt : conf var `cert_core' `cert_cot' `cert_ts' ccty , ex 

* data types 
loc datatype_vars b se ll ul pval tval 

_mpitb_rframe  , frame(fr_float) replace 			// floats?
foreach v in `datatype_vars' {
	frame fr_float: confirm float v `v'					
}

_mpitb_rframe  , frame(fr_double) double replace 		// double?
foreach v in `datatype_vars' {
	frame fr_double: confirm double v `v'					
}

e

/* debug and checks 
frame level : d 
frame level : char l 


frame post lev (1) (2) (3) (4) (5) (6) ("nat") ("H") ("") ("gmpi") ("a very long wgts sdklfjhsd") (33) (0) (.)
frame lev : d			
frame lev: li
*/

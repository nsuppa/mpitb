cscript "cert {bf:mpitb assoc}" adofiles _mpitb_assoc.ado

use data/syn_cdta , clear 

mpitb set , d1(d_cm , name(hl)) d2(d_satt , name(ed) ) d3(d_elct, name(ls)) /// 
	name(t01) 

svyset psu [pw=weight], strata(stratum)

* no err msgs 	
rcof "n mpitb assoc [aw=weight] if t==1 , dep(d_*)" == 0
rcof "n mpitb assoc [aw=weight] if t==1, name(t01)" == 0  

rcof "n mpitb assoc [aw=weight] if t==1" == 198  		// one of dep() or name() is needed
rcof "n mpitb assoc [aw=weight] if t==1 , depind(d_*) name(t01)" == 198 // only one of them 
rcof "n mpitb assoc [aw=weight] if t==1 , name(sdfsdf)" == 198 // spec not found

exit


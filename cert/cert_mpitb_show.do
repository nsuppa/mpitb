cscript "cert {bf:mpitb show}" adofiles _mpitb_show.ado

* A syntax 

use data/syn_cdta.dta , clear  

rcof n mpitb show == 197 						// only one of list and name 
rcof n mpitb show , list name(abc) == 197 

rcof n mpitb show , list == 197					// mpitb set first 

rcof n mpitb show , name(XYZ) == 197			// indicators for XYZ not set

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls)) de(banjsdbkj)

rcof n mpitb show, n(mympi) == 0			// w/o weights 

mpitb est , m(H) n(mympi) k(33) w(equal) levelfr(myres, replace)

rcof n mpitb show , name(mympi) == 0 		// with weights 
	
mpitb set , name(trial01) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls)) de(sdfsd sdfsdvjhkuiu)

mpitb set , name(trial02) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls)) de(dfygdf sdfgds)

mpitb set , name(trial03) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls)) desc(sfdgdfg dfgdfb)

mpitb show , list

* ret option
_mpitb_setwgts , name(trial1) dimw(.33333333 .33333333 .33333333) wgtsname(equal)
rcof n _mpitb_show , n(trial1) ret == 0

_mpitb_show , n(trial1) ret


exit 

/* 

This is the main certification file, which runs all other certifications scripts 

*/
adopath + ~/Work/Development/mpitb

set varabbrev off

* core
do cert/cert_mpitb.do				// syntax (+ existence of subcommands)
									// synthetic output 	--- data/syn_refsheet
									// real-world out 		---TODO---
do cert/cert_mpitb_set.do			// syntax 				--- data/syn_refsheet
do cert/cert_mpitb_est.do			// syntax 
	
* cross-country
do cert/cert_mpitb_refsh 			// some syntax 
do cert/cert_mpitb_ctyselect.do		// syntax and output		--- data/syn_refsheet

* auxiliary 
do cert/cert_mpitb_assoc			// syntax 
	// addmetaionfo 

* internal tools
do cert/cert_mpitb_setwgts		// syntax and weight assignment checks
do cert/cert_mpitb_genwgts 		// nothing 
do cert/cert_mpitb_gafvars		// syntax and output 
do cert/cert_mpitb_rframe.do	// syntax and functionality checks
do cert/cert_mpitb_stores.do	// syntax checks 
do cert/cert_mpitb_estcot.do	// some syntax checks

exit 

* show changed first lines of files changed since last tag
loc tag v0.1.2
tempfile infotxt
shell git diff tags/`tag' --name-only '*.ado' '*.sthlp' > `infotxt'

tempname fh
file open `fh' using "`infotxt'", read
file read `fh' line 
while r(eof)==0 {
		di as smcl "{hline 80}"
		display _asis `"  `macval(line)'"'
		!head -2 `line'
		file read `fh' line
		di as smcl "{hline 80}"
}
file close `fh'




exit

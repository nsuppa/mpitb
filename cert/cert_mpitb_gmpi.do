version 16.1

* create folder if not existing 
cap mkdir cert/results
cap mkdir cert/results/ctys

* glo path2gmpi ~/Work/OPHI/Cloud/Global MPI/GMPI 2021				// path to gmpi in cloud 
glo path2gmpi ~/Work/Development/gmpi/gmpi2021/						// local path

***************************
**# reference sheet CME ***
***************************
set traced 3

mpitb refsh using "cert/results/refsh_cme.dta" , clear char(ccty ccnum survey year cty class) /// 
	id(ccty) sid(region) path($path2gmpi/cdta/) gent(year) depind(d_cm d_nutr d_educ d_satt d_sani d_wtr d_ckfl d_hsg d_elct d_asst)

	
destring ccnum , replace
encode class , gen(svy_class)
drop class 

* save years separately for ext dta merge 
gen year_f = substr(year,1,4)			
gen year_l = substr(year,-4,.)			
destring year_? , replace

keep if t == T 
gen flav = "cme"

save cert/results/refsh_cme_reg , replace

* national only for merge
duplicates drop ccty t , force
drop region region_name 
save cert/results/refsh_cme , replace

***************************
**# reference sheet HOT ***
***************************

mpitb refsh using "cert/results/refsh_hot.dta" , clear char(ccty ccnum survey year cty class) /// 
	id(ccty) sid(region_01) path($path2gmpi/cdta/) depind(d_*_01) gent(year)

destring ccnum , replace
encode class , gen(svy_class)
drop class 

* hot fixes would go here 
replace  misind = regexr(misind ,"_[0-9]+","")
replace  misind = regexr(misind ,"_[0-9]+","")

* save years separately for ext dta merge 
gen year_f = substr(year,1,4)			
gen year_l = substr(year,-4,.)			
destring year_? , replace

drop if T == 1 

ren region_01* region* 

gen year_cot = (year_f + year_l)/2   			// year of survey for time period calc in change

gen flav = "hot"

sort ccty t region

* hot regional reference sheet (time-constant) 
preserve 
duplicates drop ccty region , force  
drop t T
save cert/results/refsh_hot_reg , replace
restore 

* hot national reference sheet (country-year obs)
duplicates drop ccty t , force
drop region region_name 
sort ccty t
save cert/results/refsh_hot , replace


********************************
*** compare reference sheets ***
********************************

* CME
use cert/results/refsh_cme.dta , clear 
cf ccty cty ccnum survey fname year Nind misind svy_class t T flav /// 
	using "$path2gmpi/results/dta/refsh_cme.dta" , all verb
	
use cert/results/refsh_cme_reg.dta , clear 
cf ccty cty ccnum survey fname year Nind misind svy_class t T flav /// 
	using "$path2gmpi/results/dta/refsh_cme_reg.dta" , all verb

* HOT
use cert/results/refsh_hot.dta , clear 
cf ccty cty ccnum survey fname year Nind misind svy_class t T flav /// 
	using "$path2gmpi/results/dta/refsh_hot.dta" , all verb
	
use cert/results/refsh_hot_reg.dta , clear 
cf ccty cty ccnum survey fname year Nind misind svy_class flav /// 
	using "$path2gmpi/results/dta/refsh_hot_reg.dta" , all verb

	
	
/*
mkf org_rs 
frame org_rs : use "$path2gmpi/results/dta/refsh_cme_reg.dta" , clear

frlink 1:1 ccty region , frame(org_rs)
li if mi(org_rs)
li if ccty == "GUY" 
*/

**********************************
**# load both refsh into frame ***
**********************************

cap mkf rs_cme 
frame rs_cme : use cert/results/refsh_cme.dta , clear 			

cap mkf rs_hot 
frame rs_hot : use cert/results/refsh_hot.dta , clear 			

**********************
**# CME estimation ***
**********************

cwf default
frame rs_cme : mpitb ctyselect ccty , // r(^[A])  // r(^[C-Z])
foreach cty in `r(ctylist)' {

	frame rs_cme : qui levelsof fname if ccty == "`cty'" , loc(fname) clean
	use `"$path2gmpi/cdta/`fname'"' , clear

	keep hh_id ind_id strata psu weight area region d_* dst_*	// drop vars to save mem
	svyset psu [pw=weight] , strata(strata)	singleunit(centered)	

	********************
	**** GMPI (CME) ****
	********************
	
	mpitb set , d1(d_cm d_nutr, n(hl)) d2(d_satt d_educ, n(ed)) /// 
		d3(d_elct d_sani d_wtr d_hsg d_asst d_ckfl , n(ls)) n(gmpi_cme) 

	mpitb est , svy w(equal) n(gmpi_cme) m(all) aux(all) indm(all) ts ///					
		k(1 33 100) indk(1 33 100) addmeta(ccty=`cty') double  /// 
		o(area region, k(33)) levelsa(cert/results/ctys/`cty'_cme_main , replace) 

	*******************
	**** DST (CME) ****
	*******************

	mpitb set , d1(dst_cm dst_nutr, n(hl)) d2(dst_satt dst_educ, n(ed) ) /// 			
		d3(dst_elct dst_sani dst_wtr dst_hsg dst_asst dst_ckfl , n(ls)) n(dst_cme)  

	mpitb est , k(33) w(equal) n(dst_cme) svy addmeta(ccty=`cty') ///  
		m(all) aux(all) indm(all) o(area region) double ///  
		levelsa(cert/results/ctys/`cty'_cme_dst , replace) ts 
}

**********************
**# HOT estimation ***
**********************

frame rs_hot : mpitb ctyselect ccty , // s(ALB) // s(GUY)  // r(^[R-Z]) 
foreach cty in `r(ctylist)' {
	
	* compile pooled data
	frame rs_hot : sum t if ccty == "`cty'" , mean
	loc T = r(max)
	
	clear
	gen t = .
	gen year_cot = . 
	forval t =1/`T' {
		frame rs_hot : levelsof fname if ccty == "`cty'" & t == `t' , loc(fname) clean
		frame rs_hot : levelsof year_cot if ccty == "`cty'" & t == `t' , loc(yc) clean
		tempvar app
		append using `"$path2gmpi/cdta/`fname'"' , gen(`app') nol
		replace t = `t' if `app' == 1 
		replace year_cot = `yc' if `app' == 1
	}

	keep hh_id ind_id strata psu weight area region_* t year_cot d_* dst_* 	// drop vars to save mem
	ren region_01 region 
	
	svyset psu [pw=weight] , strata(strata)	 singleunit(centered)		// svyset

	******************
	*** GMPI (HOT) ***
	******************
	mpitb set , d1(d_cm_01 d_nutr_01, n(hl)) d2(d_satt_01 d_educ_01, n(ed)) /// 
		d3(d_elct_01 d_sani_01 d_wtr_01 d_hsg_01 d_asst_01 d_ckfl_01 , n(ls)) n(gmpi_hot)

	mpitb est , k(33) w(equal) n(gmpi_hot) tvar(t) m(all) indm(all) aux(all) double  /// 
		svy addmeta(ccty=`cty') levelsa(cert/results/ctys/`cty'_hot_lev_main, replace) /// 
		cotm(all) coty(year_cot) coto(inseq nor) ts over(region area) /// 
		cotsa(cert/results/ctys/`cty'_hot_cot_main, replace)

	***************** 
	*** DST (HOT) ***
	***************** 
	mpitb set , d1(dst_cm_01 dst_nutr_01, name(hl)) d2(dst_satt_01 dst_educ_01, na(ed)) /// 
		d3(dst_elct_01 dst_sani_01 dst_wtr_01 dst_hsg_01 dst_asst_01 dst_ckfl_01 , n(ls)) n(dst_hot)

	mpitb est , k(33) w(equal) n(dst_hot) tvar(t) measures(A) over(area) /// all
		svy addmeta(ccty=`cty') levelsa(cert/results/ctys/`cty'_hot_lev_dst , replace) ts /// 
		cotm(all) coty(year_cot) coto(inseq nor) double /// 
		cotsa(cert/results/ctys/`cty'_hot_cot_dst, replace) /// 
		
}

********************************************
*** gen results_raw: level (cme and hot) *** 
********************************************

clear 
save cert/results/results_raw , replace emptyok
loc flist1 : dir "cert/results/ctys/" files "*lev*.dta"
loc flist2 : dir "cert/results/ctys/" files "*cme*.dta"
foreach f in `flist1' `flist2'  {
	append using cert/results/ctys/`f' , nol 
}

* hot fix (remove once estmation procedures allow for more vars)
gen flav = "cme" if inlist(spec,"dst_cme","gmpi_cme")
replace flav = "hot" if inlist(spec,"gmpi_hot","dst_hot") 
lab var flav "flavour (CME vs HOT)"

replace spec = "dst" if inlist(spec,"dst_cme","dst_hot")
replace spec = "gmpi" if inlist(spec,"gmpi_cme","gmpi_hot")

* gen hot indicator ( temporary )
gen ind_v = regexr(indicator,"_[0-9]+","") /* if flav == "cme"*/	// create converse & rename
ren (indicator ind_v)(ind_v indicator)

* censored potentially wrong SE		( temporary )						
replace se = .a if inlist(measure, "actb", "pctb")
replace ll = .a if inlist(measure, "actb", "pctb")
replace ul = .a if inlist(measure, "actb", "pctb")

* gen t = . // temp

order ccty flav t spec loa measure indicator 
compress 
save cert/results/results_raw , replace

**********************************
**# gen results_raw: cot (HOT) ***
**********************************

frame rs_hot {
	cap gen t0 = t 
	cap gen t1 = t
}

clear 
save cert/results/results_cot_raw , replace emptyok
loc flist : dir "cert/results/ctys/" files "*cot*.dta"
foreach f in `flist' {
	append using cert/results/ctys/`f' 
}

* hotfix 
gen flav = "hot"
lab var flav "flavour (CME vs HOT)"

replace spec = "dst" if spec == "dst_hot"
replace spec = "gmpi" if spec == "gmpi_hot"
replace k = . if measure == "vuln"		

* gen hot indicator
gen ind_v = regexr(indicator,"_[0-9]+","") 	// create converse & rename
ren (indicator ind_v)(ind_v indicator)

order ccty spec loa measure indicator ctype ann  
order cty ind_v flav , last 

compress
save cert/results/results_cot_raw , replace

*****************************************
**# compare results_raw (level & hot) ***
*****************************************

use cert/results/results_raw , clear 							// replicated gmpi 

cap mkf org_res
frame org_res: use "$path2gmpi/results/dta/results_raw.dta" , clear 			// original gmpi 

* link and get estimates
frlink 1:1 flav t spec ccty loa subg measure indicator k ctype , frame(org_res)

keep if !mi(org_res)
foreach m in b {		// se ll ul 
	frget `m'_org = `m' , from(org_res)
}

* replace b_org = b_org / 100 if inlist(measure,"H","A","sev","vuln","hd","pctb","hdk","popsh")

assert float(b_org) == float(b)

foreach m in b {		// se ll ul 
	cap drop `m'_diff_D
	cap drop `m'_diff
	cap drop `m'_diff_F
	gen double `m'_diff_D = abs(`m' - `m'_org) > 1e-08
	gen double `m'_diff_F = float(`m') - float(`m'_org) if float(`m') != float(`m'_org)
	gen double `m'_diff = abs(`m' - `m'_org)
	
}
sum b_diff if b_diff_D == 1

sum b_diff_F

br ccty k loa measure b* if b_diff 

count if b_diff_D
assert r(N) == 0 

tab measure if b_diff_D == 1

*********************************
**# compare results_raw (cot) ***
*********************************

use cert/results/results_cot_raw , clear 

cap mkf org_cot
frame org_cot: use "$path2gmpi/results/dta/results_cot_raw.dta" , clear 			// original gmpi 

* link and get estimates
frlink 1:1 t0 t1 spec ccty loa subg measure indicator k ctype , frame(org_cot)

keep if !mi(org_cot)
foreach m in b {		// se ll ul 
	frget `m'_org = `m' , from(org_cot)
}

* replace b_org = b_org / 100 if inlist(measure,"H","A","sev","vuln","hd","pctb","hdk","popsh")

assert float(b_org) == float(b)

foreach m in b {		// se ll ul 
	cap drop `m'_diff_D
	cap drop `m'_diff
	cap drop `m'_diff_F
	gen `m'_diff_D = abs(`m' - `m'_org) > 1e-018
	gen double `m'_diff_F = float(`m') - float(`m'_org) if float(`m') != float(`m'_org)
	gen double `m'_diff = abs(`m' - `m'_org)
	
}
sum b_diff if b_diff_D == 1
tab measure if !mi(b_diff_F)


***************
**# tidy up ***
***************
loc flist : dir "cert/results/ctys/" files "*.dta"
foreach f of loc flist {
	rm cert/results/ctys/`f'
}
rmdir cert/results/ctys 
loc flist : dir "cert/results/" files "*.dta"
foreach f of loc flist {
	rm cert/results/`f'
}
rmdir cert/results

exit 

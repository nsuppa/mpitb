cscript "cert {bf:mpitb refsh}" adofiles _mpitb_refsh.ado

* prepare setting 
cap mkdir data/cdta 
foreach f in ABC DEF GHI {
	use data/syn_`f'_cdta.dta   , clear 
	gen ccty = "`f'"										// to test -keep- option
	* replace ccty = "" in 1/10
	*gen survey = "DHS" 
	*replace survey = ".." in 1/10
	save data/cdta/syn_`f'_cdta.dta , replace 
}



**# A syntax 
rcof n mpitb refsh == 100 // using required 

tempfile rs  
rcof n mpitb refsh using `rs' == 198 			// id required 
rcof n mpitb refsh using `rs' , id(ccty) clear == 198 						// one of path or file option
rcof n mpitb refsh using `rs' , id(ccty) file(xxx) path(yyy) clear == 198 	// one of path or file option
rcof n mpitb refsh using `rs' , id(ccty) path(data/cdta/) == 198 // one of clear, update, newfiles is required

rcof n mpitb refsh using `rs' , id(ccty) file(xxx) newf == 198 			// file not with newf 
rcof n mpitb refsh using `rs' , id(ccty) file(xxx) update(yyy) == 198 	// file not with udpate 

rcof n mpitb refsh using `rs' , id(ccty) path(data/XXXYYYZZZ) clear == 601 // file not found
rcof n mpitb refsh using `rs' , id(ccty) path(data/cdta) newf == 601
rcof n mpitb refsh using `rs' , id(ccty) path(data/cdta) upd(ABC) == 601
rcof n mpitb refsh using `rs' , id(cctyasdf) path(data/cdta) clear == 111 

**# B output
set traced 3
set trace on 

* national level only
tempfile rs  
rcof n mpitb refsh using `rs' , id(ccty) path(data/cdta) clear keep(ccty) == 0
drop if ccty == "ABC"
save `rs' , replace 
rcof n mpitb refsh using `rs' , id(ccty) path(data/cdta) keep(ccty) upd(ABC) == 119 	// country not found if update
rcof n mpitb refsh using `rs' , id(ccty) path(data/cdta) keep(ccty) newf  == 0
save `rs' , replace 
rcof n mpitb refsh using `rs' , id(ccty) path(data/cdta) upd(ABC GHI) keep(ccty) == 0

* file option 
tempfile rs  
rcof n mpitb refsh using `rs' , id(ccty) file(data/cdta/syn_DEF_cdta) clear keep(ccty) == 0
use `rs' , clear 
d
li 

* subregions
tempfile rs  
rcof n mpitb refsh using `rs' , id(ccty) sid(region) path(data/cdta) clear keep(ccty) == 0





/*
tempfile rs  
* mpitb refsh using `rs' , rebuild id(ccty) path(data/cdta) 	== 111 // variable not found
mpitb refsh using `rs' , rebuild id(ccty) path(data/cdta) char(ccty) // region(region)
use `rs' 
li 

li ccty region region_name
*/ 

* delete files and folder 
foreach f in ABC DEF GHI {
	rm data/cdta/syn_`f'_cdta.dta 
}
rmdir data/cdta 

exit 

/* things / scenarios to test
- missing indicator 
- cot / cme 
- region missing
- no region option

*/

mpitb refsh using  mpitb_intro/results/dta/refsh.dta , rebuild id(ccty) path(cdta) 	// path not found

mpitb refsh using  mpitb_intro/results/dta/refsh.dta , rebuild id(ccty) char(ccty) path(mpitb_intro/cdta) 
		// simple national sheet

mpitb refsh using  mpitb_intro/results/dta/refsh.dta , rebuild id(ccty) char(ccty) /// 
	path(mpitb_intro/cdta) depind(d_*)
		// simple national sheet with optional missing indicator check

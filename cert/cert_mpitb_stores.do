cscript "cert {bf:mpitb stores}" adofiles _mpitb_stores.ado

******************
**# (A) syntax ***
******************

**# (A.1) setting: simple CS
clear all 

use data/syn_cdta if t == 1  , clear
_mpitb_rframe , frame(mylevs)

rcof noi _mpitb_stores , fr(mylevs) loa(nat) m(hd) spec(mympi) == 301 // last est not found


mean d_cm 

rcof "noi _mpitb_stores , fr(mylevs) loa(nat) m(hd) spec(mympi) ts" == 198		// rframe lacks ts-vars
rcof "noi _mpitb_stores , fr(mylevs) loa(nat) m(hd) spec(mympi) ctype(3)" == 198    // unknown ctype 

*rcof "noi _mpitb_stores , fr(mylevs) loa(nat) m(hd) spec(mympi) ctype(1)" == 198 	// use lincom for abs chg
*rcof "_mpitb_stores , fr(mylevs) loa(nat) m(hd) spec(mympi) ctype(2)" == 198 	// use nlcom for rel chg

rcof "noi _mpitb_stores , fr(mylevs) loa(nat) m(hd) spec(mympi) tvar(t)" == 198 	// rframe lacks tvar

_mpitb_stores , fr(mylevs) loa(nat) m(hd) spec(mympi) 

**# (A.2) setting: RCS 
use data/syn_cdta , clear

_mpitb_rframe , frame(mylevs) t replace 

mean d_cm , over(t)
_mpitb_stores , fr(mylevs) loa(nat) m(hd) spec(mympi) tvar(t)					// should work correctly
rcof "_mpitb_stores , fr(mylevs) loa(nat) m(hd) spec(mympi)" == 198 			// tvar required 

* results frame -and- store command ignore over time estimation
_mpitb_rframe , frame(mylevs) replace
mean d_cm , over(t)
rcof "_mpitb_stores , fr(mylevs) loa(nat) m(hd) spec(mympi)" == 198 			// forgot tvar option?

_mpitb_rframe , frame(mylevs) t replace 
mean d_cm 
rcof "_mpitb_stores , fr(mylevs) loa(nat) m(hd) spec(mympi) tvar(t)" == 198 	// e-cmd lacks over option

* incompatible options with ctype = 0 ("level")
_mpitb_rframe , frame(myl) cot replace
mean d_cm , over(t)
rcof "noi _mpitb_stores , fr(myl) loa(nat) m(hd) sp(mympi) tvar(t) yt0(2000)" == 198 // option not allowed with ctype=0
rcof "noi _mpitb_stores , fr(myl) loa(nat) m(hd) sp(mympi) tvar(t) yt1(2000)" == 198 
rcof "noi _mpitb_stores , fr(myl) loa(nat) m(hd) sp(mympi) tvar(t) t0(2000)" == 198 	
rcof "noi _mpitb_stores , fr(myl) loa(nat) m(hd) sp(mympi) tvar(t) t1(2000)" == 198 	

* incompatible options with ctype = 1,2 ("changes")
rcof "noi _mpitb_stores , fr(myl) loa(nat) m(hd) sp(mympi) tvar(t) ct(1)" == 198 	

* required options for ctype = 1,2 ("changes")
rcof "noi _mpitb_stores , fr(myl) loa(nat) m(hd) sp(mympi) ct(1)" == 198 	
rcof "noi _mpitb_stores , fr(myl) loa(nat) m(hd) sp(mympi) ct(1) yt0(2000)" == 198 	
rcof "noi _mpitb_stores , fr(myl) loa(nat) m(hd) sp(mympi) ct(1) yt0(2000) yt1(2005) t0(1) t1(2)" == 198 	

* invalid values in options 
est clear 
_mpitb_rframe , frame(mycots) cot replace
mean d_cm , over(t)
lincom d_cm@2.t - d_cm@1.t
rcof "noi _mpitb_stores , fr(mycots) loa(nat) m(hd) spec(mym) ct(1) yt0(1900) yt1(2000) t0(1) t1(1.2) ann(1)" == 126
rcof "noi _mpitb_stores , fr(mycots) loa(nat) m(hd) spec(mym) ct(1) yt0(1900) yt1(2000) t0(1) t1(1 2) ann(1)" == 123
rcof "noi _mpitb_stores , fr(mycots) loa(nat) m(hd) k(3.4) spec(mym) ct(1) yt0(1900) yt1(2000) t0(1) t1(1) ann(1)" == 126
rcof "noi _mpitb_stores , fr(mycots) loa(nat) m(hd) k(104) spec(mym) ct(1) yt0(1900) yt1(2000) t0(1) t1(1) ann(1)" == 125
rcof "noi _mpitb_stores , fr(mycots) loa(nat) m(hd) spec(mym) ct(1) yt0(1900) yt1(2000) t0(1) t1(1) ann(.1)" == 126
rcof "noi _mpitb_stores , fr(mycots) loa(nat) m(hd) spec(mym) ct(1) yt0(1900) yt1(2000) t0(1) t1(1) ann(2)" == 125


* no err msgs expected 
_mpitb_rframe , frame(mycots) cot replace 			
mean d_cm , over(t)
lincom d_cm@2.t - d_cm@1.t
_mpitb_stores , fr(mycots) loa(nat) m(hd) spec(mympi) ctype(1) yt0(1900) yt1(2000) t0(1) t1(2) ann(1)

_mpitb_rframe , frame(mycots) replace 							// results frame of wrong type
mean d_cm , over(t)
lincom d_cm@2.t - d_cm@1.t
rcof "_mpitb_stores , fr(mycots) loa(nat) m(hd) spec(mympi) ctype(1) yt0(1900) yt1(2000) t0(1) t1(2) ann(1)" == 198 

* supported cmds 
_mpitb_rframe , frame(mylevs) replace 
prop area
mpitb stores, frame(mylevs) loa(area) m(H) spec(mympi)

mean d_cm 
mpitb stores, frame(mylevs) loa(nat) m(H) spec(mympi) i(d_cm) 

frame mylevs : li 


******************
*** (B) output *** 
******************
frame reset 
use data/syn_cdta if t == 1  , clear
_mpitb_rframe , frame(mylevs) replace

mean d_cm 
_mpitb_stores , fr(mylevs) loa(nat) m(my_very_long_name) spec(mympi)
frame mylevs {
	 assert "`: type measure'" == "str17"
}

/*
  - test whether specified meta is really returned 
*/ 

exit

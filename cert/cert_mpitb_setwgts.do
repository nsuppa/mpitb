cscript "cert {bf:mpitb setwgts}" adofiles _mpitb_setwgts.ado

* (A.1) syntax and input
use data/syn_cdta if t == 1 , clear 

rcof n mpitb setwgts , name(t01) dimw(.3 .3) wgtsname(t1) == 197	// dep vars not set

mpitb set , d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed) ) /// 
	d3(d_elct d_sani d_wtr d_hsg d_asst d_ckfl , name(ls)) name(trial1) 

rcof n mpitb setwgts , name(trial1) dimw(.3 .3) wgtsname(w1) == 197		// #dimensions and #weights do not match!
rcof n mpitb setwgts , name(trial1) indw(.3 .3) wgtsname(w1) == 197		// #indicators and #weights do not match!

rcof n mpitb setwgts , name(trial1) dimw(.3 .3 .3) indw(.1 .2) wgtsname(t1) == 197		// indw() or dimw
rcof n mpitb setwgts , name(trial1) wgtsname(t1) == 197					// indw() or dimw

rcof n mpitb setwgts , name(trial1) dimw(.3 .3 .3) wgtsname(t1) == 197				// weights are not summing up
rcof n mpitb setwgts , name(trial1) indw(.3 .3 .3 .3 .3 .3 .3 .3 .3 .3) wgtsname(t1) == 197	// weights are not summing up
rcof n mpitb setwgts , name(trial1) indw(.2 .05 .05 .1 .1 .1 .1 .1 .1) wgtsname(t2) == 197		// # indicators > # ind wgts 
rcof n mpitb setwgts , name(trial1) indw(.1 .1 .2 .05 .05 .1 .1 .1 .1 .1 .1) wgtsname(t2) == 197	// # indicators < # ind wgts 

preserve
replace d_cm = .
replace d_nutr = . 
rcof n mpitb setwgts , name(trial1) dimw(.33333333 .33333333 .33333333) wgtsname(t1) == 197	// dim has only missing indicators!
restore

preserve
replace d_cm = .
replace d_nutr = . 
rcof n _mpitb_setwgts , name(trial1) indw(.2 .05 .05 .1 .1 .1 .1 .1 .1 .1) wgtsname(t2) == 197		// ind spec with missing indicator
mpitb show , n(trial1)
restore


****************
**# B output ***
****************

*******************
**# B.1 returns ***
*******************

***********************************************
**# (B.2) weight assignment (and precision) ***
***********************************************
* all indicator - equal-nested
_mpitb_setwgts , name(trial1) dimw(.33333333 .33333333 .33333333) wgtsname(t1) 

mat B = [1/6 , 1/6 , 1/6 , 1/6, 1/18, 1/18, 1/18, 1/18, 1/18, 1/18]'
mat li B 
mat li r(wgts_dep_m)
_massert r(wgts_dep_m) B , tol(1e-08) 			// float precision okay - PROBLEM (!?)

* all indicators - 50% health
_mpitb_setwgts , dimw(.5 .25 .25) wgtsname(t1) name(trial1)
mat B = [1/4 , 1/4 , 1/8 , 1/8, 1/24, 1/24, 1/24, 1/24, 1/24, 1/24]'
mat li B
mat li r(wgts_dep_m) 
_massert r(wgts_dep_m) B , tol(1e-08) 

* 1 missing indicator
preserve
replace d_cm = .
_mpitb_setwgts , dimw(.33333333 .33333333 .33333333) wgtsname(t1) name(trial1)	// with dimw
mat B = [1/3 , 1/6 , 1/6, 1/18, 1/18, 1/18, 1/18, 1/18, 1/18]'
mat li B 
mat li r(wgts_dep_m)
_massert r(wgts_dep_m) B , tol(1e-08) 

rcof _mpitb_setwgts , indw(.1 .1 .1 .1 .1 .1 .1 .1 .1 .1) wgtsname(t2) name(trial1)	== 197 // with dimw

restore 

* 2 missing indicators
preserve
replace d_ckfl = .
replace d_cm = .
_mpitb_setwgts , dimw(.33333333 .33333333 .33333333) wgtsname(t1) name(trial1)
mat B = [1/3 , 1/6 , 1/6, 1/15, 1/15, 1/15, 1/15, 1/15]'
mat li B 
mat li r(wgts_dep_m)
_massert r(wgts_dep_m) B , tol(1e-08) 
restore

* higher precision 
preserve
replace d_ckfl = .
replace d_cm = .
_mpitb_setwgts , dimw(`=1/3' `=1/3' `=1/3') wgtsname(t1) name(trial1)
mat B = [1/3 , 1/6 , 1/6, 1/15, 1/15, 1/15, 1/15, 1/15]'
mat li B 
mat li r(wgts_dep_m)
_massert r(wgts_dep_m) B , tol(1e-13) 
restore

exit

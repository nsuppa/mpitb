cscript "cert {bf:mpitb estcot}" adofiles _mpitb_estcot.ado

******************
**# (A) syntax ***
******************
frame reset
clear all 

use data/syn_cdta , clear 

_mpitb_rframe , frame(mycots) cot

* err msgs
rcof "noi _mpitb_estcot , fr(mycots) tvar(t) m(H) sp(mympi) y(year)" == 197				// at least one of inseq or total
rcof "noi _mpitb_estcot , fr(mycots) tvar(t) m(H) sp(mympi) y(year) inseq" == 301  		// last estimates not found

mean d_cm d_satt , over(t)
rcof "noi _mpitb_estcot , fr(mycots) tvar(t) y(year) inseq m(hd) sp(mygmpi)" == 197  	// more than 1 var in varlist

mean d_cm
rcof "noi _mpitb_estcot , fr(mycots) tvar(t) y(year) inseq m(hd) sp(mygmpi)" == 111  	// tvar not in over()

mean d_cm , over(t)
rcof "noi _mpitb_estcot , fr(mycots) tvar(t) y(year) inseq m(hd) sp(mygmpi) subgvar(area)" == 111  // 
rcof "noi _mpitb_estcot , fr(mycots) tvar(t) m(H) sp(mympi) y(year) inseq noann noraw" == 197  // at least one of ann raw

prop d_cm , over(t)
rcof "noi _mpitb_estcot , fr(mycots) tvar(t) y(year) inseq m(hd) sp(mygmpi)" == 197

* no err msgs 
mean d_cm , over(t) 
rcof "noi _mpitb_estcot , fr(mycots) tvar(t) y(year) inseq m(hd) sp(mygmpi)" == 0  	

mean d_cm , over(t area)
rcof "noi _mpitb_estcot , fr(mycots) tvar(t) y(year) inseq m(hd) sp(mygmpi) sub(area)" == 0  

mean d_cm , over(t) 
noi _mpitb_estcot , fr(mycots) tvar(t) y(year) inseq m(hd) sp(mygmpi) raw ann


******************
**# (B) output ***
******************

/* 
- abs , rel
- ann , raw
- total and inseq
*/ 

exit

********************
**# [A.1] syntax ***
********************
use data/syn_cdta.dta , clear 

mpitb set , name(mympi) d1(d_cm, name(hl)) d2(d_satt, name(ed)) /// 
	d3(d_elct, name(ls)) replace

* one of k() or cvec
rcof "n _mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt)" == 197 		


***************************
**# [A.2] existing vars ***
***************************

* extisting c_ 
_mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt) k(33) cvec indicator
rcof "n _mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt) k(33)" == 110  
drop c_eql

* extisting I_ 
rcof "n _mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt) k(33)" == 110  
drop I_33_eql c_33_eql

* extisting I_ k=100 working 
rcof "n _mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt) k(100)" == 0
drop I_100_eql c_100_eql

* existing cd_ and actb_* 
rcof "n _mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt) k(33) indicator" == 110  
drop cd_* 
rcof "n _mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt) k(33) indicator" == 110  
drop actb_*

* successful replace 
_mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt) k(33) cvec indicator
_mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt) k(33) cvec indicator replace

****************************
**# [B] output generated ***
****************************

use data/syn_cdta.dta , clear 

* variables generated - default
_mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt) k(33)
confirm var I_33_eql c_33_eql
drop I_33_eql c_33_eql

* variables generated - cvec 
_mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt) k(33) cvec 
confirm var c_eql
drop I_33_eql c_33_eql c_eql

* variables generated - indicator 
_mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt) k(33) ind
loc vlist cd_cm_33_eql actb_d_cm_33_eql cd_nutr_33_eql actb_d_nutr_33_eql cd_satt_33_eql actb_d_satt_33_eql 
confirm var `vlist'
drop `vlist'

* data type (and replace)
_mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt) k(33) replace
loc vt : type c_33_eql
assert "`vt'" == "float"
drop c_33_eql I_33_eql 

_mpitb_gafvars , indw(.1 .1 .1) wgtsid(eql) indvars(d_cm d_nutr d_satt) k(33) double
loc vt : type c_33_eql
assert "`vt'" == "double"
drop c_33_eql I_33_eql 

exit 

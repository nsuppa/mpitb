cscript "cert {bf:mpitb est}" adofiles _mpitb_est.ado

*****************************
**# [A.1] measures syntax ***
*****************************
di as res "-- [A.1] measures syntax --"
use data/syn_cdta.dta , clear 

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

**# [A.1.1] inputs

* data not svyset
rcof "n mpitb est , m(all) k(20 33 40) w(equal) n(mympi) noestimate skipgen svy" == 119	
mpitb est , m(all) k(20 33 40) w(equal) n(mympi) noestimate skipgen	// Warning 2

* data svyset, but svy not set 
svyset psu [pw=weight], strata(stratum)
rcof "n mpitb est , m(all) k(20 33 40) w(equal) n(mympi) noestimate skipgen" == 119 

* -svy- not with -if- or -in-
rcof "n mpitb est in 1/100, m(all) k(20 33 40) w(equal) n(mympi) noestimate skipgen svy" == 119 
rcof "n mpitb est if area == 1 , m(all) k(20 33 40) w(equal) n(mympi) noestimate skipgen svy" == 119

* no observations for estimation 
ren weight w1
gen weight = .
rcof "n mpitb est , m(all) k(33) w(equal) n(mympi) noestimate svy" == 2000  
drop weight 
ren w1 weight

* no measure specified 
rcof "n mpitb est , name(mympi) lfr(myres, replace) svy" == 197


* est of pctb requires M0 
rcof "n mpitb est , name(mympi) svy indm(all) k(33) w(equal)" == 198

* unknown measure 
rcof "n mpitb est , name(mympi) svy m(XYZ) k(33) w(equal)" == 198
rcof "n mpitb est , name(mympi) svy indm(XYZ) k(33) w(equal)" == 198
rcof "n mpitb est , name(mympi) svy aux(XYZ)" == 198

* measures and indm require k and wgts 
rcof "n mpitb est , name(mympi) svy m(all)" == 198				
rcof "n mpitb est , name(mympi) svy m(all) k(33)"  == 198		
rcof "n mpitb est , name(mympi) svy m(all) w(equal)"  == 198	
rcof "n mpitb est , name(mympi) svy indm(all)" == 198	

* either gen or skipgen 
rcof "n mpitb est , name(mympi) svy aux(all) gen skipgen"  == 198

**# [A.1.4] over option
rcof "n mpitb est , name(mympi) svy m(all) k(33) w(equal) lfr(myres, replace) over(, noov)" == 100
rcof "n mpitb est , name(mympi) svy m(all) k(33) w(equal) lfr(myres, replace) over(region , XYZ)" == 198
rcof "n mpitb est , name(mympi) svy m(all) k(33) w(equal) lfr(myres, replace) over(&sdfsd,)" == 198
rcof "n mpitb est , name(mympi) svy m(all) k(33) w(equal) lfr(myres, replace) over(region, k(-1))" == 125
rcof "n mpitb est , name(mympi) svy m(all) k(33) w(equal) lfr(myres, replace) over(region, k(123))" == 125
rcof "n mpitb est , name(mympi) svy m(all) k(33) w(equal) lfr(myres, replace) over(region, indk(-1))" == 125
rcof "n mpitb est , name(mympi) svy m(all) k(33) w(equal) lfr(myres, replace) over(region, indk(123))" == 125

**# [A.1.5] from -setwgts- 

* dim weights do not sum up to one
rcof "n mpitb est , m(all) k(33) n(mympi) w(dimw(.3333333 .3333333 .3333333)) svy noestimate" == 197 	
rcof "n mpitb est , m(all) k(33) n(mympi) w(dimw(.5 .3 .4)) svy noestimate" == 197 				

* too many weights
rcof "n mpitb est , m(all) k(33) n(mympi) w(dimw(.5 .3 .3 .4)) svy noestimate" == 197 	

* too few weights
rcof "n mpitb est , m(all) k(33) n(mympi) w(dimw(.5 .4)) svy noestimate" == 197 	

* dim weights out of range
rcof "n mpitb est , m(all) k(33) n(mympi) w(dimw(.2 -.4 -.4)) svy noestimate" == 197

* ind weights out of range
rcof "n mpitb est , m(all) k(33) n(mympi) w(indw(.1 .1 .1 .1 .1 .2 -.1 .1 .1 .1) name(eq)) svy noestimate" == 197 	

* ? 
rcof "n mpitb est , m(all) k(33) n(mympi) w(indw(.5 .4)) svy noestimate" == 197 		// indw() w/o name


************************
**# [A.2] COT syntax ***
************************
di as res "-- [A.2] COT syntax --"

use data/syn_cdta.dta , clear 

svyset psu [pw=weight], strata(stratum)

_mpitb_set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

* COT requires levels
rcof "n mpitb est , name(mympi) k(33) w(equal) svy lfr(myframe) cotm(XYZ) cotfr(mycot)" == 198 		

* unknown COT measure 
rcof "n mpitb est , name(mympi) m(H) k(33) w(equal) svy lfr(myframe) cotm(XYZ) cotfr(mycot)" == 198 		

* unknown COT option
rcof "n mpitb est , name(mympi) m(H) k(33) w(equal) svy lfr(myframe) cotm(H) tvar(t) cotfr(mycot) coty(year) coto(XYZ)" == 198 		

* tvar required if cot
rcof "n mpitb est , name(mympi) m(H) k(33) w(equal) svy lfr(myframe) cotm(H) cotfr(mycot)" == 198 		

* cotyear require if cot
rcof "n mpitb est , name(mympi) m(H) k(33) w(equal) svy lfr(myframe) cotm(H) cotfr(mycot) tvar(t)" == 198 

* cotmeasure w/o level
rcof "noi mpitb est , n(mympi) m(H) k(33) w(equal) svy lfr(myframe, replace) cotm(H M0) cotfr(mycot, replace) tvar(t) coty(year)" == 197 

* no error msgs
rcof noi mpitb est , n(mympi) m(H) k(33) w(equal) svy lfr(myframe, replace) cotm(H) cotfr(mycot, replace) tvar(t) coty(year) == 0 



****************************
**# [A.3] Results syntax ***
****************************
di as res "-- [A.3] Results syntax --"

* at least one of lframe or lsave
rcof "noi mpitb est , name(mympi) m(H) k(33) w(equal) svy" == 197				

* at least one of cotframe or cotsave
rcof "noi mpitb est , name(mympi) m(H) k(33) w(equal) svy lfr(myframe) cotm(H)" == 197 

******************
**# [B] output ***
******************

**************************************
**# [B.0] files and frames created ***					/* WORK IN PROGRESS */
**************************************

********************
*** B.0.1. files ***
********************

* results file already exists (testing: parser and file extension)
use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)
mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

mpitb est , name(mympi) m(M0) k(33) w(equal) svy lsa(myf.dta)				// saved w extension
rcof "n mpitb est , name(mympi) m(M0) k(33) w(equal) svy lsa(myf.dta)" == 602
rcof "n mpitb est , name(mympi) m(M0) k(33) w(equal) svy lsa(myf)" == 602
rcof "n mpitb est , name(mympi) m(M0) k(33) w(equal) svy lsa(myf, replace)" == 0	// replace works
rcof "n mpitb est , name(mympi) m(M0) k(33) w(equal) svy lsa(myf.dta, replace)" == 0
rm myf.dta 

mpitb est , name(mympi) m(M0) k(33) w(equal) svy lsa(myf)					// saved w/o extension
rcof "n mpitb est , name(mympi) m(M0) k(33) w(equal) svy lsa(myf)" == 602
rcof "n mpitb est , name(mympi) m(M0) k(33) w(equal) svy lsa(myf.dta)" == 602
rcof "n mpitb est , name(mympi) m(M0) k(33) w(equal) svy lsa(myf, replace)" == 0		// replace works
rcof "n mpitb est , name(mympi) m(M0) k(33) w(equal) svy lsa(myf.dta, replace)" == 0
rm myf.dta 

* file already exists (using tempfiles and for both level and cot files)
tempfile myfile1  myfile2 myfile3 
mpitb est , name(mympi) m(H) k(33) w(equal) svy lsa(`myfile1') 
rcof "n mpitb est , name(mympi) m(H) k(33) w(equal) svy lsa(`myfile1')" == 602 	// level file

mpitb est , name(mympi) m(H) k(33) w(equal) svy lsa(`myfile1', replace) cotm(H) tvar(t) coty(year) cotsa(`myfile2') 
rcof "n mpitb est , name(mympi) m(H)  k(33) w(equal) svy lsa(`myfile1', replace) cotm(H) tvar(t) coty(year) cotsa(`myfile2')" == 602  // cot file

mpitb est , name(mympi) m(H)k(33) w(equal) svy lfr(myframe, replace) dta(`myfile3')
mpitb est , name(mympi) m(H) k(33) w(equal) svy lfr(myframe, replace) dta(`myfile3', replace)
rcof "n mpitb est , name(mympi) m(H) k(33) w(equal) svy lsa(myframe, replace) dta(`myfile3')" == 602

********************
*** B.0.2 frames ***
********************
frame reset
use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)
mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))


* level frame already exists 
mpitb est , name(mympi) m(H) k(33) w(equal) svy lfr(myframe) 
rcof "n mpitb est , name(mympi) m(H) k(33) w(equal) svy lfr(myframe)" == 110 

* cot frame already exists 
mpitb est , name(mympi) m(H) k(33) w(equal) svy lfr(myframe, replace ) cotm(H) tvar(t) coty(year) cotfr(mycots) 
rcof "n mpitb est , name(mympi) m(H) k(33) w(equal) svy lfr(myframe, replace ) cotm(H) tvar(t) coty(year) cotfr(mycots) " == 110

*********************************
**# [B.1] variables generated ***
*********************************

use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

mpitb est , name(mympi) m(M0) indm(hdk actb pctb) k(33) w(equal) svy gen lfr(myres, replace)

*********************************************
**# B.1.1 variables are correctly created ***
*********************************************
foreach v of varlist c_equal c_33_equal actb_* pctb_* {
	confirm v `v'
}

* ind-measure-specific variables
use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

mpitb est , name(mympi) m(M0) indm(hdk) k(33) w(equal) svy gen lfr(myres, replace)
conf v cd_cm_33_equal 
conf new v actb_d_cm_33_equal
conf new v pctb_d_cm_33_equal
drop *_equal sample 
	
mpitb est , name(mympi) m(M0) indm(hdk pctb) k(33) w(equal) svy gen lfr(myres, replace)
conf v cd_cm_33_equal 
conf v pctb_d_cm_33_equal
conf new v actb_d_cm_33_equal
drop *_equal sample 

mpitb est , name(mympi) m(M0) indm(actb pctb) k(33) w(equal) svy gen lfr(myres, replace)
conf new v cd_cm_33_equal 
conf v actb_d_cm_33_equal
conf v pctb_d_cm_33_equal
drop *_equal sample 

* AF vars not for missing values in indicators 
mpitb est , name(mympi) m(M0) indm(all) k(33) w(equal) lfr(myres, replace) svy gen 
count if sample == 1 
loc N = r(N)

count if !mi(c_33_equal)
assert r(N) == `N'
count if !mi(I_33_equal)
assert r(N) == `N'

* type float?
foreach v of varlist c_equal c_33_equal actb_* pctb_* {
	confirm float v `v'
	
}
drop I_33_equal c_equal c_33_equal actb_* pctb_* cd_* sample 

* type double?
mpitb est , name(mympi) m(M0) indm(hdk actb pctb) k(33) w(equal) svy gen double lfr(myres, replace)
foreach v of varlist c_equal c_33_equal actb_* pctb_* {
	confirm double v `v'
	drop `v'
}

*************************************
**# B.1.2 variables already exist ***
*************************************
use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

mpitb est , name(mympi) m(M0) indm(hdk actb) k(33) w(equal) svy gen lfr(myres, replace)
rcof "n mpitb est , name(mympi) m(M0) indm(hdk actb) k(33) w(equal) svy gen lfr(myres, replace)" == 110 
drop sample 

rcof "n mpitb est , n(mympi) m(M0) indm(hdk actb pctb) k(33) w(equal) svy gen lfr(myres, replace)" == 110
drop c_equal

rcof "n mpitb est , n(mympi) m(M0) indm(hdk actb pctb) k(33) w(equal) svy gen lfr(myres, replace)" == 110
drop I_33_equal

rcof "n mpitb est , n(mympi) m(M0) indm(hdk actb pctb) k(33) w(equal) svy gen lfr(myres, replace)" == 110
drop c_33_equal

rcof "n mpitb est , n(mympi) m(M0) indm(hdk actb pctb) k(33) w(equal) svy gen lfr(myres, replace)" == 110
drop cd_* 

rcof "n mpitb est , n(mympi) m(M0) indm(hdk actb pctb) k(33) w(equal) svy gen lfr(myres, replace)" == 110
drop actb* 

mpitb est , n(mympi) m(M0) indm(pctb) k(33) w(equal) svy gen lfr(myres, replace)
drop sample c_equal I_* c_*
rcof "n mpitb est , n(mympi) m(M0) indm(pctb) k(33) w(equal) svy gen lfr(myres, replace)" == 110

* replace (no errors)
rcof "n mpitb est , name(mympi) m(M0) indm(hdk) k(33) w(equal) svy gen lfr(myres, replace) replace" == 0 
rcof "n mpitb est , name(mympi) m(M0) indm(hdk) k(33) w(dimw(.5 .25 .25) name(hl50)) svy gen lfr(myres, replace) replace" == 0


**************************************************
**# [B.2] generated variables in result frames ***
**************************************************
use data/syn_cdta.dta , clear 
svyset psu [pw=weight], strata(stratum)

loc datatype_vars b se ll ul pval tval 

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

* floats?
mpitb est , name(mympi) m(M0) k(33) w(equal) svy lfr(myres, replace) 
foreach v in `datatype_vars' {
	frame myres: confirm float v `v'
}

* double?
mpitb est , name(mympi) m(M0) k(33) w(equal) svy lfr(myres, replace) double
foreach v in `datatype_vars' {
	frame myres: confirm double v `v'					
}

*********************************
***# [B.3] measures estimated ***
*********************************

******************************************
*** 3.3.1 retained sample with weights ***
******************************************
use data/syn_cdta.dta , clear 
mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

mpitb est , name(mympi) aux(mv) lfr(myres, replace) 

frame myres : count if measure == "mv_w" & mi(indicator)
assert r(N) == 0 		// not calculated w/o -svy- option


svyset psu [pw=weight] , strata(stratum) singleunit(centered)
mpitb est , name(mympi) aux(mv) lfr(myres, replace) svy 
 
frame myres : count if measure == "mv_w" & mi(indicator)
assert r(N) == 1 		// calculated only -svy- option

*****************************************
*** 3.3.2 indicator-specific measures ***
*****************************************

use data/syn_cdta.dta , clear 
mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

svyset psu [pw=weight] , strata(stratum) singleunit(centered)

* using -klist- option 
mpitb est , name(mympi) weights(equal) m(M0) k(20 33 50) indm(all) lfr(myres, replace) svy
frame myres : count if measure == "M0"
assert r(N) == 3 
frame myres : count if inlist(measure,"hdk","actb","pctb") 
assert r(N) == 90

* using -indk-  option
mpitb est , name(mympi) weights(equal) m(M0) k(20 33 50) indm(hdk) indk(33)  lfr(myres, replace) svy

frame myres : count if measure == "M0"
assert r(N) == 3 
frame myres : count if measure == "hdk"
assert r(N) == 10 

****************************
*** 3.3.3 over subgroups ***
****************************
use data/syn_cdta.dta , clear 
mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))
svyset psu [pw=weight] , strata(stratum) singleunit(centered)

mpitb est , name(mympi) weights(equal) m(M0) k(33) over(region) lfr(myres, replace) svy
	
frame myres : count if measure == "M0" & loa == "region" & k==33  
assert r(N) == 20 
frame myres : count if measure == "popsh"
assert r(N) == 20

mpitb est , name(mympi) weights(equal) m(M0) indm(hdk) k(20 33 50) indk(33 50) /// 
	over(region, nooverall k(20 33) indk(33)) lfr(myres, replace) svy
frame myres {
	count if loa == "nat"
	assert r(N) == 0
	inspect k if loa == "region"
	assert r(N_unique) == 2 
	inspect k if loa == "region" & measure == "hdk"
	assert r(N_unique) == 1
}

************************************
*** 3.3.4 purged standard errors ***
************************************

use data/syn_cdta.dta if t == 1 , clear 

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

svyset psu [pw=weight] , strata(stratum) singleunit(centered)

mpitb est , name(mympi) weights(equal) m(M0) indm(actb pctb) aux(mv) k(33) over(area) /// 
	lfr(myres, replace) svy

foreach m in se ll ul {
	frame myres: count if !mi(`m') & inlist(measure,"actb","pctb","mv_w","mv_uw")
	assert r(N) == 0
}

*******************
***# [B.4] misc ***
*******************

****************************
*** 3.4.1 setting as SRS ***
****************************
use data/syn_cdta.dta , clear 
mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))


mpitb est , name(mympi) m(M0) k(33) w(equal) lfr(myres, replace) 

* mpitb est sets sampling design SRS (as follows) unless specified otherwise
svyset 
assert `"`r(settings)'"'   == `"_n, singleunit(missing) vce(linearized)"'
assert `"`r(vce)'"'        == `"linearized"'
assert `"`r(singleunit)'"' == `"missing"'
assert         r(stages_wt) == 0
assert         r(stages)    == 1

*******************************************************
*** 3.4.2 not changing whatever is specified before ***
*******************************************************
use data/syn_cdta.dta , clear 

svyset psu [pw=weight] , strata(stratum) singleunit(centered)

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_wtr d_sani d_hsg d_ckfl d_asst, name(ls))

mpitb est , name(mympi) m(M0) k(33) w(equal) lfr(myres, replace) svy

* mpitb should not touch if sth has been specified before 
svyset 
assert `"`r(settings)'"'   == `"psu [pweight= weight], strata(stratum) singleunit(centered) vce(linearized)"'
assert `"`r(strata1)'"'    == `"stratum"'
assert `"`r(su1)'"'        == `"psu"'
assert `"`r(vce)'"'        == `"linearized"'
assert `"`r(singleunit)'"' == `"centered"'
assert `"`r(wexp)'"'       == `"= weight"'
assert `"`r(wvar)'"'       == `"weight"'
assert `"`r(wtype)'"'      == `"pweight"'
assert         r(stages_wt) == 0
assert         r(stages)    == 1



***************************************************************
**# B.4.3 alternative variable names (comprehensive re-run) ***
***************************************************************
use data/syn_cdta.dta , clear 
ren (d_nutr d_cm d_satt d_educ d_elct d_sani d_wtr d_hsg d_ckfl d_asst) /// 
	(myvarA myvarB myvarC myvarD myvarE myvarF myvarG myvarH myvarI myvarJ)
	
ren (weight region psu stratum year t)(hhwgts province primsampunit STRATA annum mytime)
	// add t 

mpitb set , name(mympi) d1(myvarA myvarB , name(hl)) d2(myvarC myvarD , name(ed)) /// 
	d3(myvarE  myvarF myvarG myvarH myvarI  myvarJ  , name(ls))
	
svyset primsampunit [pw=hhwgts], strata(STRATA)

rcof n mpitb est , m(all) indm(all) k(33) w(equal) n(mympi) over(province) svy lfr(myres, replace) tv(mytime) == 0


*********************************************
**# B.4.4 principle estimation w/o svyset ***
*********************************************
use data/syn_cdta.dta , clear 

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3( d_asst d_ckfl d_hsg d_wtr d_sani d_elct , name(ls))
	
rcof n mpitb est , m(all) indm(all) k(33) w(equal) n(mympi) over(region) lfr(myres, replace) == 0


*****************************************************
**# B.4.5 ensure program completion (no aborting) ***
*****************************************************

* only aux measures with subpop (#35)
use data/syn_cdta.dta , clear 

mpitb set , name(mympi) d1(d_cm d_nutr, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3( d_asst d_ckfl d_hsg d_wtr d_sani d_elct , name(ls))
	
rcof n mpitb est , aux(all) n(mympi) over(region) lfr(myres, replace) == 0

exit 

/*
	tbc
*/ 

* svyset 
use "/home/nixon/Work/OPHI/Cloud/Global MPI/GMPI 2021/cdta/sen_dhs19.dta" , clear 
* use data/syn_cdta.dta if t == 1, clear 

mean d_cm 		// SRS

svyset _n 		// svyset version of SRS 
svy : mean d_cm 

mean d_cm [aw=`_dta[_svy_wvar]'] 

svyset psu [pw=weight] , strata(strata)		// correct svyset 
svy : mean d_cm 



svyset clear 






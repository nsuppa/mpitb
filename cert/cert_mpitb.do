cscript "cert {bf:mpitb}" adofiles mpitb.ado

*************************
**# [A] syntax issues ***
*************************

* missing subcommand 
rcof "noi mpitb " == 197

* invalid command
rcof "noi mpitb XYZ" == 197

* verfiy subcommands are called via their first error message
rcof "noi mpitb set" == 198 
rcof "noi mpitb show" == 197 
rcof "noi mpitb est" == 198 
	// mpitb est2dta

	
rcof "mpitb refsh" == 100 
rcof "mpitb ctyselect" == 100 

rcof "noi mpitb assoc" == 198 

rcof "noi mpitb setwgts" == 198 
rcof "noi mpitb gafvars" == 198 
* mpitb genwgts

rcof "noi mpitb rframe" == 198 
rcof "mpitb stores" == 198 
rcof "mpitb estcot" == 198 

******************************************
**# [B.1] synthetic output comparisons ***
******************************************

use data/syn_cdta , clear

svyset psu [pw=weight] , strat(stratum) singleu(cen)
svy : mean d_nutr

mpitb set , name(mympi) d1(d_nutr d_cm, name(hl)) d2(d_satt d_educ, name(ed)) /// 
	d3(d_elct d_sani d_wtr d_hsg d_ckfl d_asst, name(ls)) replace

mpitb est , k(20 33 50) w(equal) aux(all) me(all) indm(all) n(mympi) /// 
	svy lfr(mylevs, replace) o(area region) double 

/* compare with cert file 

frame mylevs: cf * using data/cert_syn_cdta.dta

*/

/* update cert file (to which numbers will be compared against)

frame mylevs: save data/cert_syn_cdta.dta, replace 

*/

mkf cert_synth 
frame cert_synth: use data/cert_syn_cdta , clear 

cwf mylevs 
frlink 1:1 loa measure indicator spec wgts k ctype subg , frame(cert_synth)
frget b se ll ul , from(cert_synth) suf(_crt)

* variable by variables (increase precision)
assert float(b)==float(b_crt)
assert float(se)==float(se_crt)
assert float(ll)==float(ll_crt)
assert float(ul)==float(ul_crt)

foreach m in b se ll ul {
	gen `m'_diff = abs(`m' - `m'_crt) > 1e-6
}

count if b_diff 
assert r(N) == 0 

/* explore manually

br measure loa b* if b_diff 

br measure loa b se*  if se_diff 

*/

exit 

************************************************
**# [B.1] real world data output comparisons ***
************************************************

* cme 

* hot 

* cot 


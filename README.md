Welcome to the gitlab repository of `mpitb`, a toolbox for multidimensional poverty indices for Stata.
`mpitb` is supposed to facilitate specification, estimation and analyses of MPIs and supports the popular Alkire-Foster framework to multidimensional poverty measurement.

`mpitb offers` offer several benefits to researchers, analysts and practitioners working on MPIs, including substantial time savings (e.g., due to lower data management and programming requirements) while allowing for a more comprehensive analysis at the same time.
Moreover, multidimensional poverty measurement and analysis is an active field of research, where related methods are still being refined and extended, in very different directions.
The toolbox structure of this package simplifies adding new forms of analysis to applied work.

Note that `mpitb` requires Stata version 16 or later.

# Disclaimer and limitations
This toolbox is still under active development and may thus suffer from certain limitations, including the following
1. Despite a significant amount of cross-checks, the **toolbox may still contain bugs or produce errors**. Therefore, the user is encouraged to perform occasional cross-checks, whether the reported results are as expected.
2. The toolbox may **not yet report meaningful error messages** in every instance or issue error messages relatively late (e.g., after heavy calculations).
Users who experience any such limitations or shortcomings are encouraged to report them using the [issue tracker](https://gitlab.com/nsuppa/mpitb/-/issues) of the repository.
Likewise, for feature requests or enhancements please also create issues.

Finally, the use of `mpitb` does not guarantee meaningful, informative or even correct results.
After all this remains the **responsibility of the user** and requires a proper understanding of data, methods, and analysis.
Having said that, in many instances, the toolbox does issue warnings or errors, if the user is likely to commit an error or provides implausible information.
Many issues such as coding errors in deprivation indicators or a failure to accurately specify the survey design will not be detected.

# Documentation
1. The Stata help files offer a comprehensive documentation of the toolbox. Please take the time to read them carefully.
2. The package also includes ancillary files in the `mpitb_intro` subfolder which includes a do-file with further examples for hands-on experimentation.
3. The accompanying [paper](https://ophi.org.uk/rp-62a/) provides some background on what `mpitb` actually computes and some worked examples how `mpitb` may be used.
4. You can find slides for the presentation of `mpitb` [here](https://www.stata.com/meeting/spain22/).

Finally, the roadmap for `mpitb` includes the preparation of an FAQ and a How-To site.

# Installation instructions
Depending on user needs, `mpitb` may be installed in different ways.

## 1. `ssc install`

`mpitb` is not available in the Statistical Software Components (SSC) Archive as well, yet.

## 2. `net install` from gitlab.com
You can install `mpitb` directly from this repository using Stata's `net` command.
To obtain further information about the package type in Stata
```
net describe mpitb , from("https://gitlab.com/nsuppa/mpitb/-/raw/main/")

```
To install the package type in Stata
```
net install mpitb , from("https://gitlab.com/nsuppa/mpitb/-/raw/main/")
```
The package also provides ancillary files such as an example of an analysis script and synthetic micro data).
To download these files to your _current folder_ issue
```
net get mpitb , from("https://gitlab.com/nsuppa/mpitb/-/raw/main/")
```

## 3. `git clone` the entire repository from gitlab
You can also clone the entire repository using `git`. See [here](https://git-scm.com/book/en/v2) if you wish to learn more about git.

The repository includes also includes some files used for the development of the toolbox, which are not installed by default.

To clone the repo you may use the command line or any GUI for git. After moving to the folder of your choice issue
```
git clone https://gitlab.com/nsuppa/mpitb.git
```
or, if you prefer to work with the secure shell (`ssh`)
```
git clone git@gitlab.com:nsuppa/mpitb.git
```
You can also find these links by clicking on the "Clone" button above.

Depending on the folder you cloned the repository into, you may have to tell Stata where to find it using
```
adopath + "path/to/package"
```

## 4. Download zip files and install manually
Finally, you can also download zip files and install the package manually on your machine.
You can download either download the package (as installed by `net install`) from [here](https://gitlab.com/nsuppa/mpitb/-/releases) or use the download button above left to the "Clone" button for the full repository (which also includes the development scripts).

# Uninstall `mpitb`
To uninstall `mpitb` type
```
ado uninstall mpitb
```
or in case you installed it to a particular folder issue
```
ado uninstall mpitb , from("path/to/the/toolbox")
```

# Usage of this repository
The repository has **two branches**.
Users are normally only interested in the `main` branch which is only for sharing the latest stable version of the package with end users.
Instead, the `dev` branch is used for developing the toolbox further and may contain unstable version which may be used for testing purposes but not for productive use.

The **issue tracker** of this repository may be used to report bugs and suggest enhancements or feature requests, see [here](https://gitlab.com/nsuppa/mpitb/-/issues).

If you have any **query or question**, please send a mail to [this address](mailto:contact-project+nsuppa-mpitb-24255963-issue-@incoming.gitlab.com).

# Background
This toolbox was developed together with a specific workflow for the estimation and production of the global MPI, an international poverty measure available for more than 100 countries (see [here](https://ophi.org.uk/global-mpi-2021/) for more details).
You can find more details about this workflow [here](https://www.stata.com/meeting/uk21/slides/UK21_Suppa.zip).